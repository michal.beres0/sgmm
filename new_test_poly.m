
multiply=1;
problem_type='g';
sample=1;
n_vars=4;
max_poly_degree=15;
rb_iter=40;
proposal_samples=n_vars*10;
iparsols=n_vars;

rb_eps=1e-12;
mean_sub=linspace(-15,-6,n_vars);
std_sub=ones(1,n_vars)*0.2;
diameter=std_sub(1);

%% ------------------------------------------------------------------------
script_build_problem
%% ------------------------------------------------------------------------

% RRKS params
alphas=ones(n_vars,1)*2;
type_start_RRKS='a';    % a - all, s - sum, m - mean
K0_type='m';            % o - ones, m - mean
type_build='m';         % m - MC, r - RRKS, l - LRRKS, b - combined,s - sparsegrid
type_sampler='a';       % m - mc, a - avoid, d - direct avoid
orthogonarizer_type='S';% S - SVD, G - gramm-shmidt
weight_style='s';       % s - from sol, w - from orthogonaliser, n - none
precise_sol_comp=0;


%% ------------------------------------------------------------------------
script_prepare_solvers
%% ------------------------------------------------------------------------
rng(0)
for i=(1:rb_iter)
    %PS.new_diam=PS.new_diam+i;
    if type_build=='m'
        [PS] = SGMM.SGM.enrich_RB_MC(PS,proposal,solver);
    end
    if type_build=='r'
        [PS] = SGMM.SGM.enrich_RB_RRKS(PS,proposal,solver,orthogonalizer);
    end
    
    if type_build=='b'
        [PS] = SGMM.SGM.enrich_RB_mixed(PS,proposal,solver,orthogonalizer,proposal_MC,solver_MC);
    end
    
    if type_build=='s'
        [PS] = SGMM.SGM.enrich_RB_sparse_grid(PS,solver);
    end
    
    if type_build=='l'
        [PS] = SGMM.SGM.enrich_RB_RRKSL(PS,proposal,solver,orthogonalizer);
        [PS] = SGMM.SGM.solve_RB_system_L(PS);
        [PS] = SGMM.SGM.compute_residual_L(PS);
    end
    
    if type_build=='m' || type_build=='r' || type_build=='b' || type_build=='s'
        
        [PS] = SGMM.SGM.solve_RB_system(PS);
        [PS] = SGMM.SGM.compute_residual(PS);
    end
    
    [PS] = SGMM.SGM.weights_from_sol(PS);
    
    fprintf('%d: %d\n',i,PS.last_tol)
    if PS.last_tol<rb_eps
        break;
    end
end


figure
hold on

if type_build=='s'
    plot(PS.V_size,PS.rel_res)
    set(gca,'yScale','log')
else
    
    plot((1:i),PS.rel_res)
%     plot((2:i)*iparsols,PS.rel_x_orig(2:end))
%     if precise_sol_comp==1
%         plot((1:i)*iparsols,PS.error)
%         legend({'residual','rel_x','rel_error'})
%     else
%         legend({'residual','rel_x'})
%     end
%     %plot((1:i)*iparsols,PS.rel_x2)
    set(gca,'yScale','log')
end