function [residuals_norms, results] = Reduced_solution_residual_res(A,WAW,b,Wb,W,scale_K,params)
%REDUCED_SOLUTION_RESIDUAL Summary of this function goes here
%   Detailed explanation goes here

params=exp(params)./scale_K';
n=size(params,1);
residuals_norms=zeros(n,1);
m = length(b{1});
results = zeros(m, n);
kk=length(A);
if ~isempty(W)
    for i=1:n
        tmpWAW=params(i,1)*WAW{1};
        tmpA=params(i,1)*A{1};
        tmpb=params(i,1)*b{1};
        tmpWb=params(i,1)*Wb{1};
        for k=2:kk
            tmpWAW=tmpWAW+params(i,k)*WAW{k};
            tmpA=tmpA+params(i,k)*A{k};
            tmpb=tmpb+params(i,k)*b{k};
            tmpWb=tmpWb+params(i,k)*Wb{k};
        end
        tmp_sol=W*(tmpWAW\tmpWb);
        residuals_norms(i)= norm(tmpA*tmp_sol-tmpb);
        results(:,i) = tmp_sol;
        %residuals_norms(i)= sum(abs(tmpWAW\tmpWb));
        %residuals_norms(i)= norm(W*tmpWb-tmpb);
    end
end
end

