function [ x ] = temp_precond( A,B,x )
%TEMP_PREC Summary of this function goes here
%   Detailed explanation goes here
%tic;
mm=size(A{1},1);
n=length(x)/mm;
y=reshape(x,mm,n);

A_t=A{1};

for i=2:length(A)
    A_t=A_t+A{i};
    % B_t=B_t+B{i};
end
x=A_t\y;
if 0
    B_t=0*B{1};
    for i=1:length(A)
        B_t=B_t+B{i};
    end
    
    % n=size(x,1);
    % L=ichol(B_t);
    % for i=1:n
    %     %x(i,:)=pcg(B_t,x(i,:)');
    %     [x_,flag,relres,iter,resvec]=isol.my_pcg(B_t,x(i,:)',1e-9,1000,@(x)L\(L'\x));
    %     x(i,:)=x_;
    %     %disp(iter);
    % end
    
    x=x/B_t;
end
x=x(:);
%t=toc;
%fprintf('mult time=%f \n',t)
end

