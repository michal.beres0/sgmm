function [W,iter] = sol_MC_CG(problem_setting,params)
%REDUCED_SOLUTION_RESIDUAL Summary of this function goes here
%   Detailed explanation goes here
params=exp(params)./problem_setting.scale_K';
n=size(params,1);
W=zeros(problem_setting.NK,n);
k=problem_setting.n_vars;
iter=[];
for i=1:n
    tmpA=params(i,1)*problem_setting.K{1};
    tmpb=params(i,1)*problem_setting.k{1};
    for j=2:k
        tmpA=tmpA+params(i,j)*problem_setting.K{j};
        tmpb=tmpb+params(i,j)*problem_setting.k{j};
    end
    W(:,i)=tmpA\tmpb;
    
    %prec=@(x)SGMM.FEM.AditiveSchwarzPrec(x,tmpA,problem_setting.partitions_schwarz);

    prec=SGMM.FEM.schwartz_prec(tmpA, 30,20);
    [x,flag,relres,iter1,resvec]=SGMM.isol.CG(tmpA,tmpb,1e-9,10000,prec);
    [x,iter2,resvec,tag]=SGMM.isol.DCG(tmpA,tmpb,[],problem_setting.V,[],prec,1e-9,10000);
    
    prec=@(x)diag(diag(tmpA))\x;
    [x,flag,relres,iter3,resvec]=SGMM.isol.CG(tmpA,tmpb,1e-9,10000,prec);
    [x,iter4,resvec,tag]=SGMM.isol.DCG(tmpA,tmpb,[],problem_setting.V,[],prec,1e-9,10000);
    L=ichol(tmpA);
    prec=@(x)L'\(L\x);
    [x,flag,relres,iter5,resvec]=SGMM.isol.CG(tmpA,tmpb,1e-9,10000,prec);
    [x,iter6,resvec,tag]=SGMM.isol.DCG(tmpA,tmpb,[],problem_setting.V,[],prec,1e-9,10000);    
    iter=[iter [iter1;iter2;iter3;iter4;iter5;iter6]];
end

end

