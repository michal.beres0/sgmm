function [PS] = solve_RB_system2(PS)
%SOLVE_RB_SYSTEM Summary of this function goes here
%   Detailed explanation goes here
A_sol=PS.WKW;

%%
rhs=zeros(length(PS.Wk{1}),PS.NG);
for i=1:PS.n_vars
    rhs=rhs+PS.Wk{i}*PS.g{i}';
end
%%

matmult=@(x)SGMM.SGM.temp_matmult(A_sol,PS.G,x);
prec=@(x)SGMM.SGM.temp_precond(A_sol,PS.G,x);
if ~isempty(PS.last_sol)
    tmp_=zeros(size(PS.V,2),PS.NG);
    tmp1=reshape(PS.last_sol,length(PS.last_sol)/PS.NG,PS.NG);
    tmp_(1:size(tmp1,1),:)=tmp1;
    tmp_=tmp_(:);
else
    tmp_=zeros(PS.NG*size(PS.V,2),1);
end
PS.old_sol=PS.last_sol;



if isempty(PS.iterace)
    PS.lambda = 1;
    PS.beta=2;
    PS.xi = 0.5;
    PS.test_rho = 1;
    PS.eps = PS.test_rho*PS.xi;
end


if length(PS.iterace)==1
    PS.xi = [PS.xi PS.last_tol];
    PS.test_rho = [PS.test_rho PS.test_rho/(PS.beta*sqrt(PS.lambda))];
    PS.eps = [PS.eps PS.test_rho(end)*PS.xi(end)];
    PS.gamma = [];
    PS.gamma_sum = [];
    PS.bk = [];
    PS.step_size=[];
end

if length(PS.iterace)>=2
    PS.xi = [PS.xi PS.last_tol];
    
    a=PS.eps(end-1)/PS.eps(end);
    b=PS.xi(end-1)/PS.xi(end);
    PS.bk = [PS.bk b];
    
    gamma=(a/b)^2;
    
    PS.gamma =[PS.gamma gamma];
    
    tmp=PS.gamma-1;
    
    %if sum(tmp((1+end-idx):end))>lambda
    if gamma>PS.lambda
        PS.lambda=1;
        %PS.beta=1.5;
        PS.test_rho = [PS.test_rho PS.test_rho(end)*PS.beta];
    else
        PS.test_rho = [PS.test_rho PS.test_rho(end)/(PS.beta*sqrt(PS.lambda))];
    end   
    PS.eps = [PS.eps PS.test_rho(end)*min(PS.xi)];
end

warning('off','MATLAB:nearlySingularMatrix')
 [PS.last_sol,flag,relres,iter,resvec]=SGMM.isol.CG(matmult,rhs(:),...
     PS.eps(end),10000,prec,[],tmp_);
% [PS.last_sol,flag,relres,iter,resvec]=gmres(matmult,rhs(:),100,...
% PS.eps(end)+0*PS.last_tol*1e-3,1,prec,[],tmp_);
%PS.eps_real = [PS.eps_real PS.eps(end)];
PS.eps(end)=relres;
warning('on','MATLAB:nearlySingularMatrix')
if flag ~=0
    fprintf(" flag = %d\n",flag);
end
PS.iterace=[PS.iterace;iter];
if length(PS.iterace)>=2
    step_size = norm(tmp_-PS.last_sol)/norm(tmp_);
    PS.step_size=[PS.step_size step_size];
end
end

