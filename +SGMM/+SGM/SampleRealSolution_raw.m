function [samples] = SampleRealSolution_raw(PS,input)
%SAMPLEREALSOLUTION Summary of this function goes here
%   Detailed explanation goes here
[samples_matrix,~] = SGMM.SGM.sol_MC_direct(PS,input.*PS.std_sub + PS.mean_sub);
n = size(input,1);
samples = cell(n,1);

for i = 1:n
    samples{i} = samples_matrix(:,i);
end
end

