function [matrix_vector_ids,PS] = RRKS_prop_standard(PS)
%RRKS_PROP_STANDARD Summary of this function goes here
%   Detailed explanation goes here
[~,idx]=max(PS.idx_weight);

matrix_vector_ids=zeros(PS.n_vars,2);
matrix_vector_ids(:,2)=idx;
matrix_vector_ids(:,1)=1:PS.n_vars;
PS.idx_weight(idx)=-inf;
end

