function [PS] = TT_encapsulate_problem_setting(K,k,G,g,K0,...
    poly_set,u,freeNode,mean_sub,std_sub,n_vars,scale_K,f,diameter,proposal_samples,iparsols,K0_type,alphas,K_scales,weight_style)
%ENCAPSULATE_PROBLEM_SETTING Summary of this function goes here
%   Detailed explanation goes here
PS.K=K;
PS.k=k;
PS.G=G;
PS.g=g;
PS.K0=K0;
PS.poly_set=poly_set;
PS.u=u;
PS.freeNode=freeNode;
PS.mean_sub=mean_sub;
PS.std_sub=std_sub;
PS.n_vars=n_vars;
PS.NK=length(PS.k{1});
%PS.NG=length(PS.g{1});
PS.scale_K=scale_K;
PS.f=f;
PS.diameter=[];
PS.new_diam=diameter;
PS.precise_sol=[];
PS.weight_style=weight_style;
PS.sparse_grid_lvl=0;
PS.V_size=[];


PS.alphas=alphas;
PS.scales=K_scales;

PS.WKW=cell(PS.n_vars,1);
PS.WK=cell(PS.n_vars,1);
PS.Wk=cell(PS.n_vars,1);
PS.V=[];
PS.idx_used_V=[];
PS.idx_weight=[];
PS.who_solved=[];
%PS.rhs_orig=(PS.k{1})*PS.g{1}';
if K0_type=='m'
    PS.K0=PS.K{1};
else
    PS.K0=PS.K{1}/PS.scale_K(1);
end

for i=2:(PS.n_vars)
    %PS.rhs_orig=PS.rhs_orig+(PS.k{i})*PS.g{i}';
    if K0_type=='m'
        PS.K0=PS.K0+PS.K{i};
    else
        PS.K0=PS.K0+PS.K{i}/PS.scale_K(i);
    end
end


%PS.norm_rhs_orig=norm(PS.rhs_orig(:));
PS.rel_res=[];
PS.rel_x=[];
PS.rel_x_orig=[];
PS.rel_res_orig=[];
PS.error_orig=[];
PS.rel_x2=[];
PS.error=[];
PS.iterace=[];
PS.res_norsm1=[];
PS.res_norsm2=[];
PS.last_tol=1e-2;
PS.tmp_iter=[];
PS.res_check=1;
PS.x_0_ordering=[];
PS.tmp_iter=[];
PS.iparsols=iparsols;
PS.proposal_samples=proposal_samples;
PS.iter_CG=[];
PS.last_sol=[];
PS.old_sol=[];

PS.points=[];
end

