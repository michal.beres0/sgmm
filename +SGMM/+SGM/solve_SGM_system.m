function [problem_setting] = solve_SGM_system(problem_setting)
%SOLVE_RB_SYSTEM Summary of this function goes here
%   Detailed explanation goes here
A_sol=problem_setting.K;

%%
rhs=zeros(length(problem_setting.k{1}),problem_setting.NG);
for i=1:problem_setting.n_vars
    rhs=rhs+problem_setting.k{i}*problem_setting.g{i}';
end
%%

A_prec=problem_setting.K{1};
B_prec=problem_setting.G{1};
for i=2:length(problem_setting.K)
    A_prec=A_prec+problem_setting.K{i};
    B_prec=B_prec+problem_setting.G{i};
end

matmult=@(x)my_matmult(A_sol,problem_setting.G,x);
prec=@(x)my_precond(A_prec,B_prec,x);

[rex_x,~,~,iter,resvec]=isol.my_pcg(matmult,rhs(:),1e-12,300,prec);
problem_setting.precise_sol=reshape(rex_x,problem_setting.NK,problem_setting.NG);
problem_setting.norm_precise_sol=norm(problem_setting.precise_sol);
fprintf('SMG complete solution computed in %d iterations with residual %d.\n',iter,resvec(end))
% disp(iter)
% figure
% plot(resvec);
% set(gca,'yScale','log')
%problem_setting.iterace=[problem_setting.iterace;iter];
end

function [ x ] = my_matmult( A,B,x )
mm=size(A{1},1);
n=length(x)/mm;
y=reshape(x,mm,n);
x=y*0;
for i=1:length(A)
    x=x+(A{i}*y)*B{i};
end
x=x(:);
end

function [ x ] = my_precond( A,B,x )
mm=size(A,1);
n=length(x)/mm;
y=reshape(x,mm,n);
x=(A\y)/B;
x=x(:);
end