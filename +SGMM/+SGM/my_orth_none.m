function [ W_orth,idx_orig ] = my_orth_none( V,idx_orig,W )
%MY_ORTH Summary of this function goes here
%   Detailed explanation goes here

%[ W_orth,n_temp ] = SGM.my_orth_simple( V,W );
W_orth=W;
for i=1:size(W,2)
    W_orth(:,i)=W_orth(:,i)/max(abs((W_orth(:,i))));
end

n=size(W,2);
if isempty(idx_orig)
    idx_orig=-(1:n)';
else
    idx_orig=[idx_orig ;-(1:n)'-length(idx_orig)];
end
end

