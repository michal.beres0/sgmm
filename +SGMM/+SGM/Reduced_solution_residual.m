function [residuals_norms,all_red_sols] = Reduced_solution_residual(A,WAW,b,Wb,WA,W,scale_K,params)
%REDUCED_SOLUTION_RESIDUAL Summary of this function goes here
%   Detailed explanation goes here

params=exp(params)./scale_K';
n=size(params,1);
residuals_norms=zeros(n,1);
kk=length(A);
all_red_sols = cell(n,1);

if ~isempty(W)
    for i=1:n
        tmpWAW=params(i,1)*WAW{1};
        tmpA=params(i,1)*A{1};
        tmpb=params(i,1)*b{1};
        tmpWb=params(i,1)*Wb{1};
        for k=2:kk
            tmpWAW=tmpWAW+params(i,k)*WAW{k};
            tmpA=tmpA+params(i,k)*A{k};
            tmpb=tmpb+params(i,k)*b{k};
            tmpWb=tmpWb+params(i,k)*Wb{k};
        end
        tmp_sol=W*(tmpWAW\tmpWb);
        all_red_sols{i}=tmp_sol;
        residuals_norms(i)= norm(tmpA*tmp_sol-tmpb)/norm(tmpb);
        %residuals_norms(i)= sum(abs(tmpWAW\tmpWb));
        %residuals_norms(i)= norm(W*tmpWb-tmpb);
    end
end
end

