function [PS] = enrich_RB_sparse_grid(PS,solver)



PS.sparse_grid_lvl=PS.sparse_grid_lvl+1;
[new_nodes, ~] = poly.nwspgr('KPN', PS.n_vars, PS.sparse_grid_lvl);

new_nodes=new_nodes.*PS.std_sub+PS.mean_sub;

if PS.sparse_grid_lvl==1
    points1=new_nodes;
else
    points1 = setdiff(new_nodes,PS.points,'rows');
end

PS.points=[PS.points;points1];
[W,iter_PCG] = solver(PS,points1);


PS.iter_CG=[PS.iter_CG;iter_PCG];
[PS.V,PS.WKW,PS.WK,PS.Wk] = ...
    SGM.Enrich_RB_rebuild(PS.V,W,PS.K,...
    PS.WKW,PS.WK,PS.k,PS.Wk);

PS.V_size=[PS.V_size size(PS.V,2)];
end