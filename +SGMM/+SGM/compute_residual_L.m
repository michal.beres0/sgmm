function [PS] = compute_residual_L(PS)
%COMPUTE_RESIDUAL Summary of this function goes here
%   Detailed explanation goes here

A_domain_=cell(PS.n_vars,1);
for kk=1:PS.n_vars
    A_domain_{kk}=PS.K{kk}*(PS.L'\PS.V);
end

matmult=@(x)SGMM.SGM.matmult_reduced_basis(A_domain_,PS.G,x);
x_t=matmult(PS.last_sol);

x_t=x_t(:)-PS.rhs_orig(:);

rel_res=norm(x_t)/PS.norm_rhs_orig;

PS.rel_res=[PS.rel_res;rel_res];
PS.last_tol=rel_res;


n=length(PS.last_sol)/PS.NG;
tmp_new=reshape(PS.last_sol,n,PS.NG);
if ~isempty(PS.precise_sol)
    err=norm((PS.L'\PS.V)*tmp_new-PS.precise_sol)/PS.norm_precise_sol;
end
n=length(PS.old_sol)/PS.NG;
tmp_old=reshape(PS.old_sol,n,PS.NG);
norm_new=norm(tmp_new);
tmp_new(1:n,:)=tmp_new(1:n,:)-tmp_old;


PS.rel_x=[PS.rel_x norm(tmp_new)/norm_new];
if ~isempty(PS.precise_sol)
    PS.error=[PS.error err];
end
end