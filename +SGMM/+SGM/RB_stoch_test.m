function [result] = RB_stoch_test(poly_set, points,std_sub,mean_sub,max_poly_degree, samples_sol,weights,n_used)
%RB_STOCH_TEST Summary of this function goes here
%   Detailed explanation goes here

treshold = 1e-8;
points = points(1:n_used,:);
samples_sol = samples_sol(:,1:n_used);
weights = weights(1:n_used);

[weights,idx] = sort(weights);
max_idx = n_used;%find(weights>treshold,1);

points = points(idx(1:max_idx),:);
samples_sol = samples_sol(:,idx(1:max_idx));
weights = weights(1:max_idx);

n_vars = size(poly_set,2);
n=size(points,1);
points = (points-mean_sub)./std_sub;

He = cell(n_vars, 1);

for var_i = 1:n_vars
    [He{var_i}] = SGMM.poly.HermitePoly(max_poly_degree,points(:,var_i),0,1);
end

basis_vals = zeros(size(poly_set, 1), n);

for j_n = 1:n
    values_poly = poly_set*0;
    for var_i = 1:n_vars
        val_vec = He{var_i}(:,j_n);
        values_poly(:,var_i) = val_vec(poly_set(:,var_i)+1);
    end
    basis_vals(:,j_n) = prod(values_poly, 2);
end

m=size(samples_sol,1);
n_spacial_points = 20;
spacial_idx = ceil(rand(n_spacial_points,1)*m);
values_physical = samples_sol;
%values_physical=samples_sol;
result = (basis_vals*((1./weights.^2).*basis_vals'))\(basis_vals*((1./weights.^2).*values_physical'));
%[U,D,~] = svd(result);
%max_size=find(diag(D)<1e-7,1);
%max_size = 20;
%[ base,~ ] = SGMM.SGM.my_orth_simple( [],result );
%base=U';
%D=diag(D);
end

