function [samples] = SampleSGMSolution(PS,inputs)
%SAMPLESGMSOLUTION Summary of this function goes here
%   Detailed explanation goes here
N = size(PS.V,2);
M = size(PS.poly_set,1);
U = PS.V*reshape(PS.last_sol,N,M);
[values] = SGMM.poly.CompleteHermitePolyValues(PS.poly_set,inputs);
samples_matrix = U*values;
n = size(inputs,1);
samples = cell(n,1);
nn = sqrt(length(PS.u));
for i = 1:n
    tmp = PS.u;
    tmp(PS.freeNode)=samples_matrix(:,i);
    samples{i} = reshape(tmp,nn,nn);
end
end

