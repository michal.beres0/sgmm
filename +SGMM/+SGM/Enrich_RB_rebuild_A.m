function [WAW,AW] = Enrich_RB_rebuild_A(V,A,K0,WAW,AW)
%ENRICH_RB_REBUILD Summary of this function goes here
%   Detailed explanation goes here

k=length(A);
if ~isempty(V)
    for i=1:k
        if isempty(AW{i})
            tmp12=[];
            tmp2=A{i}*(K0\V);
            tmp22=V'*tmp2;
            WAW{i}=[WAW{i} tmp12;tmp12' tmp22];
            AW{i}=tmp2;

        else
            tmp12=V'*AW{i};
            tmp2=A{i}*(K0\V);
            tmp22=V'*tmp2;
            WAW{i}=[WAW{i} tmp12';tmp12 tmp22];
            AW{i}(:,(end+1):(end+size(tmp2,2)))=tmp2;%WA{i}=[WA{i};tmp2];
        end
    end
end

end

