function [PS] = solve_RB_system_L(PS)
%SOLVE_RB_SYSTEM Summary of this function goes here
%   Detailed explanation goes here
A_sol=cell(PS.n_vars,1);

for i=1:PS.n_vars
    tmp=PS.L'\PS.V;
    A_sol{i}=tmp'*PS.K{i}*tmp;
end

%%
rhs=zeros(size(PS.V,2),PS.NG);
for i=1:PS.n_vars
    rhs=rhs+(PS.V'*(PS.L\PS.k{i}))*PS.g{i}';
end
%%

matmult=@(x)matmult_L(A_sol,PS.G,x);
if ~isempty(PS.last_sol)
    tmp_=zeros(size(PS.V,2),PS.NG);
    tmp1=reshape(PS.last_sol,length(PS.last_sol)/PS.NG,PS.NG);
    tmp_(1:size(tmp1,1),:)=tmp1;
    tmp_=tmp_(:);
else
    tmp_=zeros(PS.NG*size(PS.V,2),1);
end
PS.old_sol=PS.last_sol;
[PS.last_sol,flag,relres,iter,resvec]=SGMM.isol.CG(matmult,rhs(:),PS.last_tol*1e-3,10000,[],[],tmp_);


PS.iterace=[PS.iterace;iter];
end

function [ x ] = matmult_L( A,B,x )
%TEMP_PREC Summary of this function goes here
%   Detailed explanation goes here
tic;
mm=size(A{1},1);
n=length(x)/mm;
y=reshape(x,mm,n);
x=y*0;
for i=1:length(A)
    x=x+(A{i}*y)*B{i}; 
end
x=x(:);
t=toc;
%fprintf('mult time=%f \n',t)
end