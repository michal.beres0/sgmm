function [PS,out_cg_eps] = solve_RB_system(PS)
%SOLVE_RB_SYSTEM Summary of this function goes here
%   Detailed explanation goes here
A_sol=PS.WKW;

%%
rhs=zeros(length(PS.Wk{1}),PS.NG);
for i=1:PS.n_vars
    rhs=rhs+PS.Wk{i}*PS.g{i}';
end
%%

matmult=@(x)SGMM.SGM.temp_matmult(A_sol,PS.G,x);
prec=@(x)SGMM.SGM.temp_precond(A_sol,PS.G,x);
if ~isempty(PS.last_sol)
    tmp_=zeros(size(PS.V,2),PS.NG);
    tmp1=reshape(PS.last_sol,length(PS.last_sol)/PS.NG,PS.NG);
    tmp_(1:size(tmp1,1),:)=tmp1;
    tmp_=tmp_(:);
else
    tmp_=zeros(PS.NG*size(PS.V,2),1);
end
PS.old_sol=PS.last_sol;


beta = 2;
beta0 = 1.01;
alpha = 2;
if isempty(PS.iterace)
    PS.lambda = 100;
    PS.cur_tol=0.01;
    PS.xi = 0.9;
    PS.test_rho = 1;
    PS.eps = PS.test_rho*PS.xi;
    PS.eps_real = [];
    PS.betaplus=0.9;
end


if length(PS.iterace)==1
    PS.cur_tol=PS.cur_tol/10;
    PS.xi = [PS.xi PS.last_tol];
    PS.test_rho = [PS.test_rho 1/(beta+PS.betaplus)];
    PS.eps = [PS.eps PS.test_rho(end)*PS.xi(end)];
    PS.gamma = [];
    PS.gamma_sum = [];
    PS.bk = [];
    PS.step_size=[];
end

if length(PS.iterace)>=2
    PS.cur_tol=PS.cur_tol/max((PS.last_last_tol/PS.last_tol),1.01); 
    
    PS.xi = [PS.xi PS.last_tol];
    a=PS.eps(end-1)/PS.eps(end);
    b=max(PS.xi(end-1)/PS.xi(end),1);
    PS.bk = [PS.bk b];
    gamma=(a/b)^2;
    PS.gamma =[PS.gamma max(gamma,0)];
    tmp=PS.gamma-1;
    idx=min(1000,length(tmp));
    PS.gamma_sum=[PS.gamma_sum sum(tmp((1+end-idx):end))];
    %if sum(tmp((1+end-idx):end))>lambda
    if gamma>(PS.lambda+1)
        PS.lambda=0;
        PS.betaplus=0;
        PS.test_rho = [PS.test_rho PS.test_rho(end)*alpha];%*min(gamma,beta)
    else
        PS.test_rho = [PS.test_rho PS.test_rho(end)/(beta+PS.lambda)];
    end   
    PS.eps = [PS.eps PS.test_rho(end)*min(PS.xi)];
end

% if length(PS.iterace)>=3
%     PS.cur_tol=PS.cur_tol*10/(PS.last_last_tol/PS.last_tol)
%     PS.cur_tol=PS.cur_tol/10;    
% end
warning('off','MATLAB:nearlySingularMatrix')
 [PS.last_sol,flag,relres,iter,resvec]=SGMM.isol.CG(matmult,rhs(:),...
     PS.eps(end)*0+PS.last_tol*1e-3,10000,prec,[],tmp_);
 out_cg_eps=relres;
% [PS.last_sol,flag,relres,iter,resvec]=gmres(matmult,rhs(:),100,...
% PS.eps(end)+0*PS.last_tol*1e-3,1,prec,[],tmp_);
PS.eps_real = [PS.eps_real PS.eps(end)];
%PS.eps(end)=relres;
warning('on','MATLAB:nearlySingularMatrix')
if flag ~=0
    fprintf(" flag = %d\n",flag);
end
PS.iterace=[PS.iterace;iter];
if length(PS.iterace)>=2
    step_size = norm(tmp_-PS.last_sol)/norm(tmp_);
    PS.step_size=[PS.step_size step_size];
end
end

