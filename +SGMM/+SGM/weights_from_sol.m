function [PS] = weights_from_sol(PS)
%WEIGHTS_FROM_SOL Summary of this function goes here
%   Detailed explanation goes here


if PS.weight_style=='s'
temp_sol=reshape(PS.last_sol,size(PS.V,2),PS.NG);

PS.idx_weight=0.5*[PS.idx_weight;zeros(size(PS.V,2)-length(PS.idx_weight),1)]+0.5*sqrt(sum(temp_sol.^2,2));
end

if PS.weight_style=='w'
    PS.idx_weight=PS.idx_weight_orthogonalizer;
end

if PS.weight_style=='n'
    PS.idx_weight=[PS.idx_weight; (-(1:(size(PS.V,2)-length(PS.idx_weight)))-length(PS.idx_weight))'];
end
end

