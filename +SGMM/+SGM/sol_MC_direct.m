function [W,iter] = sol_MC_direct(problem_setting,params)
%REDUCED_SOLUTION_RESIDUAL Summary of this function goes here
%   Detailed explanation goes here
params=exp(params)./problem_setting.scale_K';
n=size(params,1);
W=zeros(problem_setting.NK,n);
k=problem_setting.n_vars;
for i=1:n
    tmpA=params(i,1)*problem_setting.K{1};
    tmpb=params(i,1)*problem_setting.k{1};
    for j=2:k
        tmpA=tmpA+params(i,j)*problem_setting.K{j};
        tmpb=tmpb+params(i,j)*problem_setting.k{j};
    end
    W(:,i)=tmpA\tmpb;
end
iter=[];
end

