function [problem_setting] = enrich_RB_RRKSL(problem_setting,proposals,solver,orthogonalizer)

[matrix_vector_ids,problem_setting]=proposals(problem_setting);


[W,iter_PCG]=solver(problem_setting,matrix_vector_ids);
problem_setting.iter_CG=[problem_setting.iter_CG;iter_PCG];

[W,tmp]=orthogonalizer(problem_setting.V,problem_setting.idx_weight,W);
problem_setting.idx_weight_orthogonalizer=tmp;




% [problem_setting.WKW,problem_setting.WK,problem_setting.Wk] = ...
%     SGM.Enrich_RB_rebuild_K(W,problem_setting.K,problem_setting.WKW,...
%     problem_setting.WK,problem_setting.k,problem_setting.Wk);
% 
% [problem_setting.WAW,problem_setting.AW] = SGM.Enrich_RB_rebuild_A(W,...
%     problem_setting.A,problem_setting.K0,problem_setting.WAW,problem_setting.AW);

problem_setting.V=[problem_setting.V W];
end