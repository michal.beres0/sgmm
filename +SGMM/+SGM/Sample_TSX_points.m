function [samples_matrix] = Sample_TSX_points(PS,N_all,mask)
%SAMPLESGMSOLUTION Summary of this function goes here
%   Detailed explanation goes here

Nvar=PS.n_vars;

data=load('+SGMM/+mat/TSX.mat');
TSX=data.TSX;
node = TSX.nodes;
elem = TSX.elems;

u0 = TSX.u0;
freeNode=u0*0+1;
freeNode(TSX.freeNode)=0;
freeNode=freeNode==1;


P1 = find(node(:,1)==50 & node(:,2)==53.25);
P2 = find(node(:,1)==50 & node(:,2)==55.75);
P3 = find(node(:,1)==(50+3.6875) & node(:,2)==50);
P4 = find(node(:,1)==(50+6.1875) & node(:,2)==50);

tmp = u0*0;
tmp(P1)=1;
tmp(P2)=1;
tmp(P3)=1;
tmp(P4)=1;

tmp2=tmp(freeNode);

idx_points = find(tmp2);

N = size(PS.V,2);
M = size(PS.poly_set,1);
U = PS.V*reshape(PS.last_sol,N,M);
U = U(idx_points,:);


batch = 1e3;
batch_count = ceil(N_all/batch);
samples_matrix=cell(1,batch_count);
for i=1:batch_count
    fprintf("\n batch %d of %d",i,batch_count);
    inputs=randn(batch,Nvar).*mask;
    [values] = SGMM.poly.CompleteHermitePolyValues(PS.poly_set,inputs);
    samples_matrix{i} = U*values;
end
samples_matrix=cell2mat(samples_matrix);
end

