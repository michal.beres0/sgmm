function [samples] = SampleRealSolution(PS,input)
%SAMPLEREALSOLUTION Summary of this function goes here
%   Detailed explanation goes here
[samples_matrix,~] = SGMM.SGM.sol_MC_direct(PS,input.*PS.std_sub + PS.mean_sub);
n = size(input,1);
samples = cell(n,1);
nn = sqrt(length(PS.u));
for i = 1:n
    tmp = PS.u;
    tmp(PS.freeNode)=samples_matrix(:,i);
    samples{i} = reshape(tmp,nn,nn);
end
end

