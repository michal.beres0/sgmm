function [matrix_vector_ids,PS] = RRKS_prop_minres(PS)
%RRKS_PROP_STANDARD Summary of this function goes here
%   Detailed explanation goes here
n=size(PS.V,2);
res=zeros(n,PS.n_vars);
for i=1:PS.n_vars
    %res(:,i)=sqrt(sum((PS.AW{i}*(PS.WAW{i}\eye(n))-PS.V).^2));
    res(:,i)=sqrt(sum((PS.K0\(PS.A{i}*PS.V*(((PS.K0\PS.V)'*PS.A{i}*PS.V)\PS.V'*PS.V))-PS.V).^2)).*PS.idx_weight';
    %res(:,i)=sqrt(sum((PS.K0\(PS.A{i}*PS.V*(((PS.K0\PS.V)'*PS.A{i}*PS.V)\PS.V'*PS.V))-PS.V).^2));
    %res(:,i)=sqrt(sum(((PS.A{i}*PS.V*((PS.V'*PS.A{i}*PS.V)\PS.V'*PS.K0*PS.V))-PS.K0*PS.V).^2));
end
[~,idx]=sort(res(:),'descend');
imagesc(res)
drawnow
matrix_vector_ids=[floor((idx-1)/n)+1 mod(idx-1,n)+1];
matrix_vector_ids=matrix_vector_ids(1:PS.iparsols,:);
end

