function [samples] = TT_SampleSGMSolution_raw(PS,inputs,x,max_poly_degree,n_vars)
%SAMPLESGMSOLUTION Summary of this function goes here
%   Detailed explanation goes here
N = size(PS.V,2);
%U = PS.V*reshape(PS.last_sol,N,M);
[values] = SGMM.poly.TT_HermitePolyValues(max_poly_degree,n_vars,inputs);



n = size(inputs,1);
samples = cell(n,1);
for i = 1:n
    MAT = tt_matrix([{eye(N)}; values(:,i)]);
    samples{i} = PS.V*full(MAT*x);
end
end