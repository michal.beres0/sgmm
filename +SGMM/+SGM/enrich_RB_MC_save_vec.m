function [PS] = enrich_RB_MC_save_vec(PS,sampler,solver)

[points1,PS]=sampler(PS);
[residuals_norms_orig,results] = SGMM.SGM.Reduced_solution_residual_res(PS.K,...
    PS.WKW,PS.k,...
    PS.Wk,PS.V,PS.scale_K,points1);

PS.new_points = points1;
PS.new_residuals = residuals_norms_orig;
PS.new_results = results;

residuals_norms=(residuals_norms_orig.^2).*PS.f(points1);
[residuals_norms,idx_greatest_res]=sort(residuals_norms,'descend');

PS.res_norsm2=[PS.res_norsm2 residuals_norms(1)];
PS.res_norsm1=[PS.res_norsm1 max(residuals_norms_orig)];
points1=points1(idx_greatest_res,:);

PS.points=[PS.points;points1(1:PS.iparsols,:)];

[W,iter_PCG] = solver(PS,points1(1:PS.iparsols,:));
PS.iter_CG=[PS.iter_CG;iter_PCG];
[PS.V,PS.WKW,PS.WK,PS.Wk] = SGMM.SGM.Enrich_RB_rebuild(PS.V,W,PS.K,...
    PS.WKW,PS.WK,PS.k,PS.Wk);
PS.diameter=[PS.diameter;PS.new_diam*ones(PS.iparsols,1)];
end