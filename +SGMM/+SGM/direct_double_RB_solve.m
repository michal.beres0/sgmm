function [u] = direct_double_RB_solve(PS, V)
%DIRECT_DOUBLE_RB_SOLVE Summary of this function goes here
%   Detailed explanation goes here
A=kron(V'*PS.G{1}*V,PS.WKW{1});
b=kron(V'*PS.g{1},PS.Wk{1});
for i=2:PS.n_vars
    A=A+kron(V'*PS.G{i}*V,PS.WKW{i});
    b=b+kron(V'*PS.g{i},PS.Wk{i});
end
u=A\b;
end

