function [W,iter] = sol_RRKS_CG(PS,midx)
%REDUCED_SOLUTION_RESIDUAL Summary of this function goes here
%   Detailed explanation goes here
n=size(midx,1);
W=zeros(PS.NK,n);
iter=[];
for i=1:n
    W(:,i)=(PS.K{midx(i,1)}*PS.scales(midx(i,1))+PS.alphas(midx(i,1))*PS.K0)\(PS.K0*PS.V(:,midx(i,2)));
      
    tmpA=(PS.K{midx(i,1)}*PS.scales(midx(i,1))+PS.alphas(midx(i,1))*PS.K0);
    tmpb=(PS.K0*PS.V(:,midx(i,2)));
    
    prec=SGMM.FEM.schwartz_prec(tmpA, 30,20);
    [x,flag,relres,iter1,resvec]=SGMM.isol.CG(tmpA,tmpb,1e-9,10000,prec);
    [x,iter2,resvec,tag]=SGMM.isol.DCG(tmpA,tmpb,[],PS.V,[],prec,1e-9,10000);
    
    prec=@(x)diag(diag(tmpA))\x;
    [x,flag,relres,iter3,resvec]=SGMM.isol.CG(tmpA,tmpb,1e-9,10000,prec);
    [x,iter4,resvec,tag]=SGMM.isol.DCG(tmpA,tmpb,[],PS.V,[],prec,1e-9,10000);
    L=ichol(tmpA);
    prec=@(x)L'\(L\x);
    [x,flag,relres,iter5,resvec]=SGMM.isol.CG(tmpA,tmpb,1e-9,10000,prec);
    [x,iter6,resvec,tag]=SGMM.isol.DCG(tmpA,tmpb,[],PS.V,[],prec,1e-9,10000);    
    iter=[iter [iter1;iter2;iter3;iter4;iter5;iter6]];
end
% [~,idx]=max(n_i);
% W=W(:,idx);
% fprintf('\n');
%iter=[];
end

