function [res,flag,relres,iter,resvec] = solve_RB_system_single(PS,G,g,eps,n_iter,W)
%SOLVE_RB_SYSTEM Summary of this function goes here
%   Detailed explanation goes here
if nargin==6
    PS.Wk = cell(PS.n_vars,1);
    PS.WKW = cell(PS.n_vars,1);
    for i=1:PS.n_vars
        PS.Wk{i} = W'*PS.k{i};
        PS.WKW{i} = W'*PS.K{i}*W;
    end
end
if isempty(G)
    G=PS.G;
    g=PS.g;
end
A_sol=PS.WKW;

%%
rhs=zeros(length(PS.Wk{1}),length(g{1}));
for i=1:PS.n_vars
    rhs=rhs+PS.Wk{i}*g{i}';
end
%%
matmult=@(x)SGMM.SGM.temp_matmult(A_sol,G,x);
prec=@(x)SGMM.SGM.temp_precond(A_sol,G,x);

[res,flag,relres,iter,resvec]=SGMM.isol.CG(matmult,rhs(:),...
    eps,n_iter,prec);

end

