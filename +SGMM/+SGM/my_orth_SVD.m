function [ W_orth,idx_orig ] = my_orth_SVD( V,idx_orig,W )
%MY_ORTH Summary of this function goes here
%   Detailed explanation goes here

%[ W_orth,n_temp ] = SGM.my_orth_simple( V,W );
for i=1:size(W,2)
    W(:,i)=W(:,i)/norm(W(:,i));
end

if ~isempty(V)
    tmp=W;
    tmp=tmp-V*(tmp'*V)';
    tmp=tmp-V*(tmp'*V)';
    tmp=tmp-V*(tmp'*V)';
else
    tmp=W;
end
[U,D,V]=svd(tmp,'econ');

idx_new=diag(D);
rel=max(idx_new);

W_orth=U(:,(idx_new>=rel/1e9));
n_temp=1:size(W_orth,2);
idx_new=idx_new(1:size(W_orth,2));

idx_orig=[idx_orig;idx_new];

end

