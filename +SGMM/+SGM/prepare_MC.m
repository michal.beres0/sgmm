function [problem_setting] = prepare_MC(iparsols,proposal_samples,problem_setting)
%PREPARE_MC Summary of this function goes here
%   Detailed explanation goes here
problem_setting.WKW=cell(problem_setting.n_vars,1);
problem_setting.WK=cell(problem_setting.n_vars,1);
problem_setting.Wk=cell(problem_setting.n_vars,1);
problem_setting.V=[];

rhs_orig=zeros(problem_setting.NK,problem_setting.NG);
for i=1:(problem_setting.n_vars)
    rhs_orig=rhs_orig+(problem_setting.k{i})*problem_setting.g{i}';
end

problem_setting.rhs_orig=rhs_orig;
problem_setting.norm_rhs_orig=norm(problem_setting.rhs_orig(:));
problem_setting.rel_res=[];
problem_setting.rel_x=[];
problem_setting.error=[];
problem_setting.iterace=[];
problem_setting.res_norsm1=[];
problem_setting.res_norsm2=[];
problem_setting.last_tol=1e-2;
problem_setting.tmp_iter=[];
problem_setting.res_check=1;
problem_setting.x_0_ordering=[];
problem_setting.tmp_iter=[];
problem_setting.iparsols=iparsols;
problem_setting.proposal_samples=proposal_samples;
problem_setting.iter_CG=[];
problem_setting.last_sol=[];
problem_setting.old_sol=[];

problem_setting.points=[];

%problem_setting.points=problem_setting.mean_sub;

% V=New_solutions(problem_setting.K,problem_setting.k,problem_setting.scale_K,problem_setting.mean_sub);
% V=V/norm(V);
% [problem_setting.V,problem_setting.WKW,problem_setting.WK,problem_setting.Wk] = ...
%     Enrich_RB_rebuild([],V,problem_setting.K,WKW,WK,problem_setting.k,Wk);
end

