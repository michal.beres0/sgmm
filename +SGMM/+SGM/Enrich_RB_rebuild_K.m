function [WAW,WA,Wb] = Enrich_RB_rebuild_K(V,A,WAW,WA,b,Wb)
%ENRICH_RB_REBUILD Summary of this function goes here
%   Detailed explanation goes here

k=length(A);
if ~isempty(V)
    for i=1:k
        if isempty(WA{i})
            tmp12=[];
            tmp2=V'*A{i};
            tmp22=tmp2*V;
            WAW{i}=[WAW{i} tmp12';tmp12 tmp22];
            WA{i}=[WA{i};tmp2];
            Wb{i}=[Wb{i};V'*b{i}];
        else
            tmp12=WA{i}*V;
            tmp2=V'*A{i};
            tmp22=tmp2*V;
            WAW{i}=[WAW{i} tmp12;tmp12' tmp22];
            WA{i}((end+1):(end+size(tmp2,1)),:)=tmp2;%WA{i}=[WA{i};tmp2];
            Wb{i}=[Wb{i};V'*b{i}];
        end
    end
end

end

