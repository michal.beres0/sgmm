function [residuals_norms_all_n,results_all_n,points_all_n] = compress_stoch_RB(residuals_norms_all, results_all, points_all, residuals_norms_all_loc, ...
    results_all_loc, points_all_loc, pattern_stoch_RB, max_size)
%COMPRESS_STOCH_RB Summary of this function goes here

residuals_norms_all_n=[residuals_norms_all; residuals_norms_all_loc];
results_all_n=[results_all results_all_loc(pattern_stoch_RB,:)];
points_all_n=[points_all;points_all_loc];

treshold = sqrt(1000)*quantile(residuals_norms_all_n,0.25);

[residuals_norms_all_n,idx] = sort(residuals_norms_all_n);

tmp = find(residuals_norms_all_n>treshold,1);
if isempty(tmp)
   tmp = length(residuals_norms_all_n);
end

max_idx = min(tmp,max_size);

residuals_norms_all_n=residuals_norms_all_n(1:max_idx);
points_all_n = points_all_n(idx(1:max_idx),:);
results_all_n = results_all_n(:,idx(1:max_idx));

end