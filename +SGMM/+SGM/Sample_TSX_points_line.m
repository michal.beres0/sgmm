function [samples_matrix] = Sample_TSX_points_line(PS,N_all)
%SAMPLESGMSOLUTION Summary of this function goes here
%   Detailed explanation goes here

mat=SGMM.mat.TSX_points_close_tunel(300);

Nvar=PS.n_vars;

data=load('+SGMM/+mat/TSX.mat');
TSX=data.TSX;
node = TSX.nodes;
elem = TSX.elems;

u0 = TSX.u0;
freeNode=u0*0+1;
freeNode(TSX.freeNode)=0;
freeNode=freeNode==1;


N = size(PS.V,2);
M = size(PS.poly_set,1);
U = PS.V*reshape(PS.last_sol,N,M);

U_large=zeros(length(u0),size(U,2));
U_large(:,1)=u0;

U_large(freeNode,:)=U;

U=mat*U_large;

batch = 1e3;
batch_count = ceil(N_all/batch);
samples_matrix=cell(1,batch_count);
fprintf("\n");
for i=1:batch_count
    fprintf("batch %d of %d \n",i,batch_count);
    inputs=randn(batch,Nvar);
    [values] = SGMM.poly.CompleteHermitePolyValues(PS.poly_set,inputs);
    samples_matrix{i} = U*values;
end
samples_matrix=cell2mat(samples_matrix);
end

