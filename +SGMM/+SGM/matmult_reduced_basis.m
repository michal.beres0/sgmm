function [ x ] = matmult_reduced_basis( A,B,x )
%TEMP_PREC Summary of this function goes here
%   Detailed explanation goes here
tic;
% n=length(x)/229441;
% y=reshape(x,229441,n);
mm=size(A{1},2);
n=length(x)/mm;
y=reshape(x,mm,n);
x=zeros(size(A{1},1),n);
for i=1:length(A)
    x=x+A{i}*(y*B{i}); 
end
x=x(:);
t=toc;
%fprintf('mult time=%f \n',t)
end

