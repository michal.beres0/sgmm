function [W,iter] = sol_RRKS_direct(PS,midx)
%REDUCED_SOLUTION_RESIDUAL Summary of this function goes here
%   Detailed explanation goes here
n=size(midx,1);
W=zeros(PS.NK,n);
for i=1:n
    W(:,i)=(PS.K{midx(i,1)}*PS.scales(midx(i,1))+PS.alphas(midx(i,1))*PS.K0)\(PS.K0*PS.V(:,midx(i,2)));
      
%     A=PS.K{midx(i,1)}*PS.scales(midx(i,1))+PS.alphas(midx(i,1))*PS.K0;
%     b=PS.K0*PS.V(:,midx(i,2));
%     n_i(i)=norm(A*PS.V*((PS.V'*A*PS.V)\(PS.V'*b))-b);
%     fprintf('%f ',n_i(i));
    %tmp=sum(problem_setting.V(:,max(end-5,1):end),2);
    %W(:,i)=problem_setting.A{matrix_vector_ids(i,1)}\(problem_setting.K0*tmp);
    %W(:,i)=problem_setting.A{matrix_vector_ids(i,1)}\(problem_setting.V(:,matrix_vector_ids(i,2)));
end
% [~,idx]=max(n_i);
% W=W(:,idx);
% fprintf('\n');
iter=[];
end

