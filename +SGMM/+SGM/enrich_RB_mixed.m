function [PS] = enrich_RB_mixed(PS,proposals,solver,orthogonalizer,proposals_MC,solver_MC)

min_buffer=1;
max_buffer=1;

if length(PS.rel_x)>=(1+2*min_buffer)
    n_buffer=min(max_buffer,floor((length(PS.rel_x)-1)/2));
    %differences=PS.rel_x2(1:end-1)-PS.rel_x2(2:end);
    
    differences=PS.rel_x_orig(2:end);
    differences_cut=differences;%max(differences((2*n_buffer):end),0);
    diff_1=differences_cut(PS.who_solved(2:end)==1);
    diff_0=differences_cut(PS.who_solved(2:end)==0);
    
    rrks_w=mean(diff_1((end-n_buffer+1):end));
    mc_w=mean(diff_0((end-n_buffer+1):end));
    if rrks_w>=mc_w
        tag=1;
    else
        tag=0;
    end
    fprintf('%d - %d -- %d \n',rrks_w,mc_w, tag)
else
    tag=mod(length(PS.rel_x),2)==1;
end


if tag==0
    PS.who_solved=[PS.who_solved;0];
    [points1,PS]=proposals_MC(PS);
    [residuals_norms_orig] = SGMM.SGM.Reduced_solution_residual(PS.K,...
        PS.WKW,PS.k,...
        PS.Wk,PS.V,PS.scale_K,points1);
    residuals_norms=residuals_norms_orig.*PS.f(points1);
    [residuals_norms,idx_greatest_res]=sort(residuals_norms,'descend');
    
    PS.res_norsm2=[PS.res_norsm2 residuals_norms(1)];
    PS.res_norsm1=[PS.res_norsm1 max(residuals_norms_orig)];
    points1=points1(idx_greatest_res,:);
    
    PS.points=[PS.points;points1(1:PS.iparsols,:)];
    
    [W,iter_PCG] = solver_MC(PS,points1(1:PS.iparsols,:));
    PS.iter_CG=[PS.iter_CG;iter_PCG];
    
    PS.diameter=[PS.diameter;PS.new_diam*ones(PS.iparsols,1)];
else
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    PS.who_solved=[PS.who_solved;1];
    
    [matrix_vector_ids,PS]=proposals(PS);
    
    
    [W,iter_PCG]=solver(PS,matrix_vector_ids);
    PS.iter_CG=[PS.iter_CG;iter_PCG];
    %W=[W W_MC];
end
[W,tmp]=orthogonalizer(PS.V,PS.idx_weight,W);
PS.idx_weight_orthogonalizer=tmp;

[PS.WKW,PS.WK,PS.Wk] = ...
    SGMM.SGM.Enrich_RB_rebuild_K(W,PS.K,PS.WKW,...
    PS.WK,PS.k,PS.Wk);

% [problem_setting.WAW,problem_setting.AW] = SGM.Enrich_RB_rebuild_A(W,...
%     problem_setting.A,problem_setting.K0,problem_setting.WAW,problem_setting.AW);

PS.V=[PS.V W];
end