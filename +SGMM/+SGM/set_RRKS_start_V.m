function [PS] = set_RRKS_start_V(PS,type,orthogonalizer)
%SET_RRKS_START_V Summary of this function goes here
%   Detailed explanation goes here

if type=='s'
    tmp_V=PS.k{1};
    for i=2:PS.n_vars
        tmp_V=tmp_V+PS.k{i};   
    end
    tmp_V=PS.K0\tmp_V;
end

if type=='a'
    tmp_V=cell2mat(PS.k);
    tmp_V=PS.K0\tmp_V;
end

if type=='m'
    [tmp_V] = SGMM.SGM.sol_MC_direct(PS,PS.mean_sub);
end

tmp_V=tmp_V(:,sum(abs(tmp_V))>0);

[V,idx_weight]=orthogonalizer([],[],tmp_V);
PS.V=V;
PS.idx_weight_orthogonalizer=idx_weight;

% for i=1:PS.n_vars
%    PS.A{i}=PS.K{i}*scales(i)+alphas(i)*PS.K0; 
% end

PS.WAW=cell(PS.n_vars,1);
PS.AW=cell(PS.n_vars,1);

[PS.WKW,PS.WK,PS.Wk] = ...
    SGMM.SGM.Enrich_RB_rebuild_K(V,PS.K,PS.WKW,...
    PS.WK,PS.k,PS.Wk);

% [PS.WAW,PS.AW] = SGM.Enrich_RB_rebuild_A(V,...
%     PS.A,PS.K0,PS.WAW,PS.AW);

end

