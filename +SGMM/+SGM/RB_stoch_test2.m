function [base] = RB_stoch_test2(poly_set, points,std_sub,mean_sub,max_poly_degree, samples_sol,weights,bsize)
%RB_STOCH_TEST Summary of this function goes here
%   Detailed explanation goes here

% treshold = 10*quantile(weights,0.01);
% 
% [weights,idx] = sort(weights);
% max_idx = find(weights>treshold,1);
% 
% points = points(idx(1:max_idx),:);
% samples_sol = samples_sol(:,idx(1:max_idx));
%weights = weights(1:max_idx);

poly_set_orig_size = size(poly_set,1);
a=300;
%b=length(weights);
%x_tmp = 1/2*(a^2 + 2*b - sqrt(a^2*(a^2 + 4*b)));
%x_tmp=b/10;

trs=quantile(1./weights.^2,0.75);
tmp=1./weights.^2;
tmp(tmp>trs)=trs;
tmp=tmp/trs;
x_tmp=sum(tmp)/2;
n_base = min(max(floor(x_tmp),1),size(poly_set,1));
poly_set = poly_set(1:n_base,:);

n_vars = size(poly_set,2);
n=size(points,1);
points = (points-mean_sub)./std_sub;

He = cell(n_vars, 1);

for var_i = 1:n_vars
    [He{var_i}] = SGMM.poly.HermitePoly(max_poly_degree,points(:,var_i),0,1);
end

basis_vals = zeros(size(poly_set, 1), n);

for j_n = 1:n
    values_poly = poly_set*0;
    for var_i = 1:n_vars
        val_vec = He{var_i}(:,j_n);
        values_poly(:,var_i) = val_vec(poly_set(:,var_i)+1);
    end
    basis_vals(:,j_n) = prod(values_poly, 2);
end

result = (basis_vals*((1./weights.^2).*basis_vals'))\(basis_vals*((1./weights.^2).*samples_sol'));
%result = (basis_vals*basis_vals')\(basis_vals*samples_sol');
[U,D,~] = svds(result,bsize,'largest','Tolerance',1e-16);

% tmp = 1-cumsum(diag(D).^2)/sum(diag(D).^2);
% idx = find(tmp<1e-17,1);
% base=U(1:idx,:)';
base=U;
base=[base;zeros(poly_set_orig_size-n_base,size(base,2))];
%D=diag(D);
end

