function [ x ] = temp_matmult( A,B,x )
%TEMP_PREC Summary of this function goes here
%   Detailed explanation goes here
%tic;
mm=size(A{1},1);
n=length(x)/mm;
y=reshape(x,mm,n);
x=y*0;
for i=1:length(A)
    x=x+(A{i}*y)*B{i}; 
end
x=x(:);
%t=toc;
%fprintf('mult time=%f \n',t)
end

 