function [PS] = enrich_RB_MC(PS,sampler,solver)

[points1,PS]=sampler(PS);
[residuals_norms_orig,all_red_sols] = SGMM.SGM.Reduced_solution_residual(PS.K,...
    PS.WKW,PS.k,...
    PS.Wk,PS.WK,PS.V,PS.scale_K,points1);
residuals_norms=(residuals_norms_orig.^2).*PS.f(points1);
[residuals_norms,idx_greatest_res]=sort(residuals_norms,'descend');
%fprintf('%f',max(residuals_norms))


mu=mean(log(residuals_norms_orig.^2));
ss=std(log(residuals_norms_orig.^2));
n=length(residuals_norms_orig);

reduced_sol = cell2mat(all_red_sols(idx_greatest_res(1:PS.iparsols))');

%reduced_sol = cell2mat(all_red_sols(idx_greatest_res)');

mu_up = mu+tinv(0.9747,n)*ss/sqrt(n);
ss_up = (n-1)*ss*ss/chi2inv(1-0.9747,n);


PS.res_norsm1=[PS.res_norsm1 sqrt(exp(mu_up+ss_up/2))];
points1=points1(idx_greatest_res,:);

PS.points=[PS.points;points1(1:PS.iparsols,:)];

[W,iter_PCG] = solver(PS,points1(1:PS.iparsols,:));
%[W,iter_PCG] = solver(PS,points1);
PS.res_norsm2=[PS.res_norsm2 sqrt(exp(mean(log(SGMM.FEM.compute_H1_norm(reduced_sol,W,PS)))))];



tmp_points=points1(1:PS.iparsols,:);
errors = SGMM.FEM.compute_H1_norm(reduced_sol,W,PS);

if PS.iparsols>1
    error_fin = sqrt(sum(PS.f(tmp_points)./mvnpdf(tmp_points,mean(tmp_points),diag(var(tmp_points))).*errors)...
        /sum(PS.f(tmp_points)./mvnpdf(tmp_points,mean(tmp_points),diag(var(tmp_points)))));
    
    PS.res_norsm3=[PS.res_norsm3 error_fin];
end

%W=W(:,1:PS.iparsols);
%iter_PCG=iter_PCG(1:PS.iparsols);
PS.samples_MC = [PS.samples_MC W];
PS.iter_CG=[PS.iter_CG;iter_PCG];
[PS.V,PS.WKW,PS.WK,PS.Wk] = SGMM.SGM.Enrich_RB_rebuild(PS.V,W,PS.K,...
    PS.WKW,PS.WK,PS.k,PS.Wk);
PS.diameter=[PS.diameter;PS.new_diam*ones(PS.iparsols,1)];
end