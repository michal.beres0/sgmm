function [residual_norm] = compute_residual_raw(PS, solution)
%COMPUTE_RESIDUAL Summary of this function goes here
%   Detailed explanation goes here

residual=solution*0;
for kk=1:PS.n_vars
    residual=residual + PS.K{kk}*solution*PS.G{kk}-PS.k{kk}*PS.g{kk}';
end

residual_norm=norm(residual);