function [PS] = start_RB_MC(PS,solver)

points1=PS.mean_sub;
PS.points=[PS.points;points1];

[W,~] = solver(PS,points1);
PS.samples_MC = W;
[PS.V,PS.WKW,PS.WK,PS.Wk] = ...
    SGMM.SGM.Enrich_RB_rebuild(PS.V,W,PS.K,...
    PS.WKW,PS.WK,PS.k,PS.Wk);
PS.diameter=[PS.diameter;PS.new_diam];
end