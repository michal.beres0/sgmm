function [residual_norm] = compute_residual_doubleRB(PS, solution,V2)
%COMPUTE_RESIDUAL Summary of this function goes here
%   Detailed explanation goes here

x_t=zeros(length(PS.k{1}),length(PS.g{1}));
for kk=1:PS.n_vars
    x_t=x_t + (PS.K{kk}*PS.V)*reshape(solution,size(PS.V,2),size(V2,2))*(V2'*PS.G{kk});
end
residual = x_t(:) - PS.rhs_orig(:);
residual_norm=norm(residual);