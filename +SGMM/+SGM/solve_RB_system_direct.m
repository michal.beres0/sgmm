function [PS] = solve_RB_system_direct(PS)
%SOLVE_RB_SYSTEM Summary of this function goes here
%   Detailed explanation goes here
A=kron(PS.G{1},PS.WKW{1});
b=kron(PS.g{1},PS.Wk{1});
for i=2:PS.n_vars
    A=A+kron(PS.G{i},PS.WKW{i});
    b=b+kron(PS.g{i},PS.Wk{i});
end
PS.last_sol=A\b;
end