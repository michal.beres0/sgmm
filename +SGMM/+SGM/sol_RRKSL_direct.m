function [W,iter] = sol_RRKSL_direct(PS,matrix_vector_ids)
%REDUCED_SOLUTION_RESIDUAL Summary of this function goes here
%   Detailed explanation goes here
n=size(matrix_vector_ids,1);
W=zeros(PS.NK,n);
for i=1:n
    W(:,i)=PS.A{matrix_vector_ids(i,1)}\(PS.L*PS.V(:,matrix_vector_ids(i,2)));
    W(:,i)=PS.L'*W(:,i);
    %tmp=sum(problem_setting.V(:,max(end-5,1):end),2);
    %W(:,i)=problem_setting.A{matrix_vector_ids(i,1)}\(problem_setting.K0*tmp);
    %W(:,i)=problem_setting.A{matrix_vector_ids(i,1)}\(problem_setting.V(:,matrix_vector_ids(i,2)));
end
iter=[];
end

