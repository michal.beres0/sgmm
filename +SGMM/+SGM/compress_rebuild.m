function [PS] = compress_rebuild(PS)
%COMPRESS_REBUILD Summary of this function goes here
%   Detailed explanation goes here
%PS_old=PS;
temp_sol=reshape(PS.last_sol,size(PS.V,2),PS.NG);

[U,D,V]=svd(temp_sol,'econ');
idx_w=diag(D);
idx_w=cumsum(idx_w(end:-1:1));
idx_w=idx_w/idx_w(end);

idx_keep=idx_w>=1e-8;
idx_keep=idx_keep(end:-1:1);

new_sol=D(idx_keep,idx_keep)*V(:,idx_keep)';
new_V=PS.V*U(:,idx_keep);
tmp_idx_weight=PS.idx_weight;
%tmp_idx_weight(tmp_idx_weight<0)=-1;
%new_tmp_idx=(tmp_idx_weight'*abs(U(:,idx_keep)))';


if isempty(PS.idx_used_V)
    PS.idx_used_V=ones(size(PS.V,2),1);
end
if length(PS.idx_used_V)~=size(PS.V,2)
    PS.idx_used_V=[PS.idx_used_V;ones(size(PS.V,2)-length(PS.idx_used_V),1)];
end

%tmp=abs(U(:,idx_keep))./sum(abs(U(:,idx_keep)));
tmp=abs(U(:,idx_keep));
PS.idx_used_V(tmp_idx_weight<0)=0;
PS.idx_used_V=(PS.idx_used_V'*tmp)';
PS.idx_used_V=max(PS.idx_used_V,0);

%VK=PS.K0\new_V;
for i=1:PS.n_vars
    PS.WK{i}=new_V'*PS.K{i};
    PS.Wk{i}=new_V'*PS.k{i};
    PS.WKW{i}=PS.WK{i}*new_V;  
end

PS.V=new_V;
PS.last_sol=new_sol(:);
PS.idx_weight=PS.idx_used_V.*diag(D(idx_keep,idx_keep));
figure(11)
hold on
plot(PS.idx_weight)
% [PS] = SGM.solve_RB_system(PS);
% temp_sol=reshape(PS.last_sol,size(PS.V,2),PS.NG);

end

