function [PS] = compute_residual(PS)
%COMPUTE_RESIDUAL Summary of this function goes here
%   Detailed explanation goes here

A_domain_=cell(PS.n_vars,1);
for kk=1:PS.n_vars
    A_domain_{kk}=PS.K{kk}*PS.V;
end

matmult=@(x)SGMM.SGM.matmult_reduced_basis(A_domain_,PS.G,x);
x_t=matmult(PS.last_sol);

x_t=x_t(:)-PS.rhs_orig(:);

rel_res=norm(x_t)/PS.norm_rhs_orig;

PS.rel_res=[PS.rel_res;rel_res];
PS.rel_res_orig=[PS.rel_res_orig;norm(x_t)];
PS.last_last_tol=PS.last_tol;
PS.last_tol=rel_res;


n=length(PS.last_sol)/PS.NG;
tmp_new=reshape(PS.last_sol,n,PS.NG);
if ~isempty(PS.precise_sol)
    err=norm(PS.V*tmp_new-PS.precise_sol)/PS.norm_precise_sol;
    PS.error_orig=[PS.error_orig norm(PS.V*tmp_new-PS.precise_sol)];
end
n=length(PS.old_sol)/PS.NG;
tmp_old=reshape(PS.old_sol,n,PS.NG);
norm_new=norm(tmp_new);
tmp_new(1:n,:)=tmp_new(1:n,:)-tmp_old;

PS.rel_x=[PS.rel_x norm(tmp_new)/norm_new];
PS.rel_x_orig=[PS.rel_x_orig norm(tmp_new)];

if ~isempty(PS.rel_x2)
    if PS.rel_x(end)<1e-3*PS.rel_xtemp(end)
        PS.rel_xtemp(end+1)=PS.rel_xtemp(end);
    else
        PS.rel_xtemp(end+1)=PS.rel_x(end);
    end
else
    PS.rel_xtemp(1)=PS.rel_x(1);
end

len_mask=3;
tmp_mask=exp(-(1:len_mask).^2/len_mask);
tmp_mask=tmp_mask/sum(tmp_mask);
tmp=conv([PS.rel_xtemp(1)*ones(len_mask,1);PS.rel_xtemp'],tmp_mask);
%tmp(len_mask+1:end-len_mask+1)
PS.rel_x2=tmp(len_mask+1:end-len_mask+1);

if ~isempty(PS.precise_sol)
    PS.error=[PS.error err];
end
end