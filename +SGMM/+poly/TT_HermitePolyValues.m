function [values] = TT_HermitePolyValues(max_degree,dim,inputs)
%COMPLETEHERMITEPOLYVALUES Summary of this function goes here
%   Detailed explanation goes here

if size(inputs,2) ~= dim
    error('Incompatibile matix sizes.');
end

n_points = size(inputs,1);

values1D = cell(dim,1);
for i = 1:dim
    values1D{i} = SGMM.poly.HermitePoly(max_degree,inputs(:,i),0,1);
end

values = cell(dim,n_points);
for i=1:n_points
    for j=1:dim
        values{j,i}=values1D{dim-j+1}(:,i)';
    end
end

end