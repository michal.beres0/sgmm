function [values] = CompleteHermitePolyValues(poly,inputs)
%COMPLETEHERMITEPOLYVALUES Summary of this function goes here
%   Detailed explanation goes here
n = size(poly,1);
dim = size(poly,2);
if size(inputs,2) ~= dim
    error('Incompatibile matix sizes.');
end
max_degree = max(poly(:));
n_points = size(inputs,1);

values1D = cell(dim,1);
for i = 1:dim
    values1D{i} = SGMM.poly.HermitePoly(max_degree,inputs(:,i),0,1);
end

values = zeros(n,n_points);

for i = 1:n
    tmp_val = ones(1,n_points);
    for j = 1:dim
        tmp_val = tmp_val.*values1D{j}(poly(i,j)+1,:);
    end
    values(i,:) = tmp_val;
end

end