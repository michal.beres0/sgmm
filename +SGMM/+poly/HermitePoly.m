function [He] = HermitePoly(n,x,mu,sigma)
%% [He] = HermitePoly(n,x,mu,sigma)
% evaluates Hermite polinomails up to degree n in points x
% with respect to normal distribution with given mu and sigma
%%

[mm,nn]=size(x);
if mm>nn
    x=x';
end

if nargin>=3
    x=x-mu;
    x=x/sigma;
end

He=zeros(n+1,length(x));
He(1,:)=1;
He(2,:)=x;

for i=2:n
    He(i+1,:)=x.*He(i,:)-(i-1)*He(i-1,:);
end

for i=0:n
    He(i+1,:)=He(i+1,:)/sqrt(factorial(i));
end

end