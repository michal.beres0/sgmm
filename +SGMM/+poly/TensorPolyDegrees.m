function [ poly ] = TensorPolyDegrees( dim, degree )
%GENERATE_POLYNOMIALS_DEGREE Summary of this function goes here
%   Detailed explanation goes here

scheme = (0:degree)';
n=length(scheme);
tmp=scheme;
for i=2:dim
    tt=repmat(tmp,n,1);
    tt2=repmat(scheme',size(tmp,1),1);
    tmp=[tt2(:) tt];
end
poly=tmp;
end