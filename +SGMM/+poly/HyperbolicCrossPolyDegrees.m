function [ poly ] = HyperbolicCrossPolyDegrees( dim, degree )
%GENERATE_POLYNOMIALS_DEGREE Summary of this function goes here
%   Detailed explanation goes here

[ poly ] = SGMM.poly.CompletePolyDegrees( dim,min(degree,1) );
idx = prod(poly+1,2)<=(degree+1);
poly=poly(idx,:);


for i=2:degree
    n=size(poly,1);
    tmp_add = [];
    for j=1:n
        tmp = poly(j,:)+eye(dim);
        idx = prod(tmp+1,2)<=(i+1);
        tmp_add = [tmp_add;tmp(idx,:)];
    end
    poly = unique([poly;tmp_add],'rows');
end

end

