function [ precond ] = schwartz_prec( A, domains,size_step )
%SCHWARTZ_PREC Summary of this function goes here
%   Detailed explanation goes here
n=size(A,1);
steps=domains;
A_inv=cell(steps,1);

ncol=round(sqrt(n))-1;
nrow=round(sqrt(n))+1;




start=0;
stop=ncol/size_step;
shift_step = (ncol-ceil(ncol/size_step))/(steps-1);
starts=zeros(steps,1);
stops=zeros(steps,1);
for i=1:steps
    start_n=round(start)*nrow+1; 
    starts(i)=start_n;
    stop_n=round(stop)*nrow;
    stops(i)=stop_n;
    A_inv{i}=(A(start_n:stop_n,start_n:stop_n));
    start=start+shift_step;
    stop=min(stop+shift_step,ncol);
end

precond=@(x)schwartz_prec_func( x,A_inv, starts,stops );
end


function [ y ] = schwartz_prec_func( x,A_inv, starts,stops )
%SCHWARTZ_PREC Summary of this function goes here
%   Detailed explanation goes here
n=length(x);
steps=length(starts);
y=zeros(n,1);
for i=1:steps
    start=starts(i);
    stop=stops(i);
    y(start:stop)=y(start:stop)+A_inv{i}\x(start:stop);
end

%y=y./double;
end