function [K,k,u,freeNode,mean_field,node,elem] = assemble_FEM_Lab_Example(materials,multiply,scale)
%ASSEMBLE_FEM_LAB_EXAMPLE Summary of this function goes here
%   Detailed explanation goes here
n_vars=length(materials);
if nargin<3
   scale=ones(n_vars,1); 
end

Dirichlet_windows=...
    [3   0     1     1
     1   0     1     0];

f=@(x)(x(:,1)*0);             % zatizeni
g_N=@(x)(x(:,1)*0);           % Neumann
h_elem=100*multiply;
[node,elem,bdFlag]=SGMM.FEM.rect_mesh(1,1,100*multiply,100*multiply);        % triangulace
[u,freeNode]=SGMM.FEM.FEM_struct( node, elem, h_elem, bdFlag, Dirichlet_windows);

K=cell(n_vars,1);
k=cell(1,n_vars);
for i=1:n_vars
    [K{i},k{i}]=SGMM.FEM.FEM( node, elem, h_elem, bdFlag, materials{i}(:)*scale(i), f, Dirichlet_windows, g_N );
end
mean_field=K{1};
for i=2:n_vars
    mean_field=mean_field+K{i};
end
end

