function [ A,b] = FEM_raw( node, elem, freeNode, k,u0)
%FEM solution of -div(k*grad(p))=f
%   node ... coordinates of te nodes
%   elem ... vertices of the elements
%   dbFlag ... types of the sides
%               0 ... non-boundary side
%               1 ... Dirichlet boundary condition
%               2 ... Neumann boundary condition

N=size(node,1);     % number of nodes
NT=size(elem,1);    % number of elements

%% VECTORIZATION (EDGES + AREA)
ve=zeros(NT,2,3);
ve(:,:,1)=node(elem(:,3),:)-node(elem(:,2),:);
ve(:,:,2)=node(elem(:,1),:)-node(elem(:,3),:);
ve(:,:,3)=node(elem(:,2),:)-node(elem(:,1),:);

area=0.5*abs(-ve(:,1,3).*ve(:,2,2)+ve(:,2,3).*ve(:,1,2));

%% USING THE SYMMETRY + PER PARTES
ii=zeros(NT,1,'double'); 
jj=zeros(NT,1,'double');
A=sparse(N,N);
for i=1:3
    ii(:)=elem(:,i);
    sA=k.*dot(ve(:,:,i),ve(:,:,i),2)./(4*area);
    A=A+sparse(ii,ii,sA,N,N);
end
for i=1:2
    for j=i+1:3
        ii(:)=elem(:,i);
        jj(:)=elem(:,j);
        sA=k.*dot(ve(:,:,i),ve(:,:,j),2)./(4*area);
        A=A+sparse(ii,jj,sA,N,N);
        A=A+sparse(jj,ii,sA,N,N);
    end
end

clear ve ii jj sA

b=-A*u0;
A=A(freeNode,freeNode);
b=b(freeNode);
end

