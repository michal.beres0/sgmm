function [K,k,u0,freeNode,mean_field,node,elem] = assemble_FEM_TSX(scale)
%ASSEMBLE_FEM_LAB_EXAMPLE Summary of this function goes here
%   Detailed explanation goes here

data=load('+SGMM/+mat/TSX.mat');
TSX=data.TSX;

n_vars=length(scale);
if nargin<3
   scale=ones(n_vars,1); 
end

%freeNode=TSX.freeNode;

u0 = TSX.u0;
freeNode=u0*0+1;
freeNode(TSX.freeNode)=0;
freeNode=freeNode==1;
K=cell(n_vars,1);
k=cell(1,n_vars);
for i=1:n_vars
    [K{i},k{i}]=SGMM.FEM.FEM_raw( TSX.nodes, TSX.elems, freeNode, (TSX.mat_id==i)*scale(i),u0);
end
mean_field=K{1};
for i=2:n_vars
    mean_field=mean_field+K{i};
end
node = TSX.nodes;
elem = TSX.elems;
end

