function [y] = AditiveSchwarzPrec(x,A,partitioning)
%ADITIVESCHWARZPREC Summary of this function goes here
%   Detailed explanation goes here
n=length(partitioning);
part_res=cell(n,1);
for i=1:n
    part_res{i}=A(partitioning{i},partitioning{i})\x(partitioning{i});
    
end
y=x*0;
for i=1:n
   y(partitioning{i})=y(partitioning{i})+part_res{i};
end

end

