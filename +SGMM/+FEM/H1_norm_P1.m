function [res_M] = H1_norm_P1(node,elem)
%H1_NORM_P1 Summary of this function goes here
%   Detailed explanation goes here
%% Discretization size and problem setting
quad_poly_degree=10;

a_weak=@(v1,v2,data)TEFEM.op.dot(v1.grad,v2.grad)+v1.val.*v2.val;

basis1=TEFEM.fe.P1();

%% triangulation 
tri_grid=TEFEM.grid.mesh_refactor(node, elem);

%% domain integration
[qd] = TEFEM.grid.quad_point_triangles(tri_grid, quad_poly_degree);
[bf1] = basis1.val(tri_grid,qd);

%% assembling matrices and vectors
[res_M] = TEFEM.assemble.bilinear(a_weak,bf1,bf1,[],qd);
end

