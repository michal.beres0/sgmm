function [partitions_schwarz]=prepare_schwarz(elems,partitions,overlap,freeNode) 
elems=double(elems);
n=max(elems(:));
m=length(elems);

B=sparse([1:m 1:m 1:m],double([elems(:,1) elems(:,2) elems(:,3)]),ones(3*m,1),m,n);
W=(B*B')>=2;
opts.seed=1;
opts.niter=100;
opts.ncuts=20;
[map,~] = SGMM.mat.metismex('PartGraphRecursive',W,partitions,opts);

partitions_schwarz = cell(partitions,1);
for i=1:partitions
    tmp_idx=double((map==(i-1)));
    for j=1:overlap
        tmp_idx=tmp_idx*W;     
    end
    tmp_idx=(tmp_idx*B)>0;
    partitions_schwarz{i}=tmp_idx(freeNode);
end

end