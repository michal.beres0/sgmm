function [res] = compute_H1_norm(a,b,PS)
%COMPUTE_H1_NORM Summary of this function goes here
%   Detailed explanation goes here

u=zeros(length(PS.u),1);
n=size(a,2);
res=zeros(n,1);
for i=1:n
u(PS.freeNode)=a(:,i)-b(:,i);
res(i)=u'*PS.H1_norm*u;
end

end

