function [u,freeNode] = FEM_struct( node, elem, h_elem, bdFlag, Dirichlet_windows )
%FEM solution of -div(k*grad(p))=f
%   node ... coordinates of te nodes
%   elem ... vertices of the elements
%   dbFlag ... types of the sides
%               0 ... non-boundary side
%               1 ... Dirichlet boundary condition
%               2 ... Neumann boundary condition

N=size(node,1);     % number of nodes
%% BOUNDARY EDGES EXTRACTION
totalEdge=[elem(:,[2,3]); elem(:,[3,1]); elem(:,[1,2])];
downEdge=totalEdge(bdFlag(:)==1,:);    down=(bdFlag==1);   t_down=2*ones(h_elem,1);  v_down=zeros(h_elem,1);  i_down=v_down;
rightEdge=totalEdge(bdFlag(:)==2,:);   right=(bdFlag==2); t_right=2*ones(h_elem,1); v_right=zeros(h_elem,1); i_right=v_right;
upEdge=totalEdge(bdFlag(:)==3,:);      up=(bdFlag==3); t_up=2*ones(h_elem,1);    v_up=zeros(h_elem,1);    i_up=v_up;
leftEdge=totalEdge(bdFlag(:)==4,:);    left=(bdFlag==4); t_left=2*ones(h_elem,1);  v_left=zeros(h_elem,1);  i_left=v_left;
values_D=zeros(N,1);

%% Dirichletova okna ------------------------------------------------
for i=1:size(Dirichlet_windows,1)
    a_=Dirichlet_windows(i,2);
    b_=Dirichlet_windows(i,3);
    okna_stred=(node(2:(h_elem+1),2)+node(1:(h_elem),2))/2;
    temp_=(1:h_elem)';
    
    okno=temp_((okna_stred>a_)&(okna_stred<b_));
    v=Dirichlet_windows(i,4);
    switch Dirichlet_windows(i,1)
        case 1	% okno dole
            o=downEdge(okno,:);
            values_D(o(:))=v;
            t_down(okno)=1; v_down(okno)=v; i_down(okno)=i;
        case 2  % okno vpravo
            o=rightEdge(okno,:);
            values_D(o(:))=v;
            t_right(okno)=1; v_right(okno)=v; i_right(okno)=i;
        case 3  % okno nahore
            o=upEdge(okno,:);
            values_D(o(:))=v;
            t_up(okno)=1; v_up(okno)=v; i_up(okno)=i;
        case 4  % okno vlevo
            o=leftEdge(okno,:);
            values_D(o(:))=v;
            t_left(okno)=1; v_left(okno)=v; i_left(okno)=i;
    end
end
bdFlag(down) =t_down;
bdFlag(right)=t_right;
bdFlag(up)   =t_up;
bdFlag(left) =t_left;

Dirichlet=totalEdge(bdFlag(:)==1,:);

u=values_D;
%% DIRICHLET BOUNDARY CONDITIONS
isBdNode=false(N,1);
isBdNode(Dirichlet)=true;
freeNode=find(~isBdNode);
end

