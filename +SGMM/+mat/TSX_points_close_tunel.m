function [mat] = TSX_points_close_tunel(n)
%TSX_POINTS_CLOSE_TUNEL Summary of this function goes here
%   Detailed explanation goes here

data=load('+SGMM/+mat/TSX.mat');
TSX=data.TSX;

y=linspace(51.75, 53.25,n);
x=y;
x(:)=50;

bounding_box = TSX.nodes(:,1)>49.5 & TSX.nodes(:,1)<50.5 & TSX.nodes(:,2)>51.5 & TSX.nodes(:,2)<54;

inelems= bounding_box(TSX.elems(:,1)) & bounding_box(TSX.elems(:,2)) & bounding_box(TSX.elems(:,3));
elems_valid = TSX.elems(inelems,:);

m=size(elems_valid,1);
mat = zeros(n,size(TSX.nodes,1));
for i=1:n
    px=x(i);
    py=y(i);
    for j=1:m
        p1=elems_valid(j,1);
        p2=elems_valid(j,2);
        p3=elems_valid(j,3);
        px1=TSX.nodes(p1,1);
        py1=TSX.nodes(p1,2);
        px2=TSX.nodes(p2,1);
        py2=TSX.nodes(p2,2);
        px3=TSX.nodes(p3,1);
        py3=TSX.nodes(p3,2);
        res = pointInTriangle(px1, py1, px2, py2, px3, py3, px, py);
        if res   
            lambda1=((py2-py3)*(px-px3)+(px3-px2)*(py-py3))/((py2-py3)*(px1-px3)+(px3-px2)*(py1-py3));
            lambda2=((py3-py1)*(px-px3)+(px1-px3)*(py-py3))/((py2-py3)*(px1-px3)+(px3-px2)*(py1-py3));
            lambda3=1-lambda1-lambda2;  
            mat(i,p1)=lambda1;
            mat(i,p2)=lambda2;
            mat(i,p3)=lambda3;
            %fprintf('Point %d in triangle %d with weights %f, %f, %f \n', i, j,lambda1,lambda2,lambda3);
            break
        end
        if j==m
            fprintf('ERROR\n');
        end
    end
end


end


function [tag] = pointInTriangle(x1, y1, x2, y2, x3, y3, x, y)

denominator = ((y2 - y3)*(x1 - x3) + (x3 - x2)*(y1 - y3));
a = ((y2 - y3)*(x - x3) + (x3 - x2)*(y - y3)) / denominator;
b = ((y3 - y1)*(x - x3) + (x1 - x3)*(y - y3)) / denominator;
c = 1 - a - b;

tag = 0 <= a && a <= 1 && 0 <= b && b <= 1 && 0 <= c && c <= 1;

end

