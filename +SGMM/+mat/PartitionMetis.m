function [materials]=PartitionMetis(elems,partitions,sample) 
n=max(elems(:));
m=length(elems);

B=sparse([1:m 1:m 1:m],double([elems(:,1) elems(:,2) elems(:,3)]),ones(3*m,1),m,n);
W=(B*B')>=2;
opts.seed=sample;
opts.niter=10;
opts.ncuts=2;
if partitions>1
    [map,~] = SGMM.mat.metismex('PartGraphRecursive',W,partitions,opts);
else
    map=zeros(length(elems),1);
end

materials=cell(partitions,1);

for i=1:partitions
    materials{i}=double(map==(i-1));
end
end
