function [materials] = PartitionGRF(multiply, partitions, sample)
%MATERIAL_PARTITIONS Summary of this function goes here
%   Detailed explanation goes here

sample=SGMM.mat.PartitionSamplesGRF(sample,partitions);
m_kron=ones(multiply*2,multiply);
sample=kron(sample,m_kron);

materials=cell(partitions,1);

for i=1:partitions
    materials{i}=double(sample==i);
end
end