function [sample,means,stds] = PartitionSamplesGRF(sample_no,npart)
%PARTITION_FROM_SAMPLES Summary of this function goes here
%   Detailed explanation goes here
fulpath=mfilename('fullpath');
filename=mfilename();
path=fulpath(1:end-length(filename));
S=load([path 'samples.mat']);
sample=S.samples{sample_no};

mi = quantile(sample(:),0.01);
ma = quantile(sample(:),0.99);

sample = sample - mi;
sample = sample/(ma - mi);
sample = sample*npart;
sample = ceil(sample);
sample(sample<1) = 1;
sample(sample>npart) = npart;
means=zeros(npart,1);
stds=zeros(npart,1);
sample_orig=S.samples{sample_no};
for i=1:npart
    means(i)=mean(sample_orig(sample==i));
    stds(i)=std(sample_orig(sample==i));
end
end

