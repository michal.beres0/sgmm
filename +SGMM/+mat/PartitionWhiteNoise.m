function [materials] = PartitionWhiteNoise(multiply, partitions, seed)
%MATERIAL_PARTITIONS Summary of this function goes here
%   Detailed explanation goes here
rng(seed);
sample=ceil(rand(100)*partitions);
m_kron=ones(multiply*2,multiply);
sample=kron(sample,m_kron);

materials=cell(partitions,1);

for i=1:partitions
    materials{i}=double(sample==i);
end
end