function [materials] = PartitionWells(multiply,partition)
%MATERIAL Summary of this function goes here
%   m ... material na oblastech (posledni okraj)
%   b ... velikost okraje k vykresleni
w=[1 1 2 2 3 3 6 6 7 7
    1 1 2 2 3 3 6 6 7 7
    1 2 2 2 3 3 6 6 7 7
    2 2 2 2 3 3 6 6 6 6
    3 3 3 3 3 3 6 6 6 6
    3 3 3 3 3 3 6 6 6 6
    4 4 4 4 3 3 3 3 3 3
    4 4 4 4 4 3 3 3 3 3
    4 4 5 5 5 5 5 5 4 4
    5 5 5 5 5 5 5 5 4 4]; % 4 a 8 stejny material

w(w>partition)=partition;

w=kron(w,ones(20*multiply,10));

materials=cell(partition,1);

for i=1:partition
    materials{i}=double(w==i);
end
end