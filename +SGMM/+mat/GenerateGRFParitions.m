function [field_split] = GenerateGRFParitions(autocovariance,grid_size,n_dubdomains, sample)
%% GENERATE_MATERIAL_PARTITION
% autocovariance = @(x,y)exp(-x.^2-y.^2)
% grid_size
% number_of_subdomains
% sample

%%
rng(sample)
x=linspace(0,3,grid_size);
y=linspace(0,3,grid_size);

[xx,yy]=meshgrid(x,y);
f=autocovariance(xx(:),yy(:));
C=zeros(length(xx(:)));
for i=1:length(yy(:))
    C(i,:)=autocovariance(xx(:)-xx(i),yy(:)-yy(i));
end

[U,D]=eigs(C,20);

L=U*diag(sqrt(diag(D)));
Z=randn(size(L,2),1);
field=reshape(L*Z,grid_size,grid_size);

quantiles=zeros(n_dubdomains+1,1);
quantiles(1)=-inf;
quantiles(end)=inf;
for i=1:(n_dubdomains-1)
    quantiles(i+1)=quantile(field(:),i/n_dubdomains);
end

field_split=field;
for i=1:n_dubdomains
    field_split(field>=quantiles(i) & field<quantiles(i+1))=i;
end

end