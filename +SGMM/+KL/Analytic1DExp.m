function [ eig_value,eig_func,coeff] = Analytic1DExp( n,sigma,lambda )
% KL_1D Analytic solution of cov(x,y) = sigma*exp(abs(x-y)/lambda)
L=1;
[ w ] = roots_1D( n,lambda);

eig_value=zeros(n,1);
eig_func_cos=zeros(n,1);
eig_func_sin=zeros(n,1);
eig_func_x=zeros(n,1);
eig_func=cell(n,1);
for i=1:n
    eig_value(i)=(2*lambda*sigma^2)/(lambda^2*w(i)^2+1);
    eig_func_x(i)=w(i);
    eig_func_sin(i)=1/sqrt((lambda^2*w(i)^2+1)*L/2+lambda);
    eig_func_cos(i)=eig_func_sin(i)*lambda*w(i);
    eig_func{i}=@(x)eig_func_sin(i)*sin(x*eig_func_x(i))+...
        eig_func_cos(i)*cos(x*eig_func_x(i));
end
coeff.eig_value=eig_value;
coeff.eig_func_x=eig_func_x;
coeff.eig_func_sin=eig_func_sin;
coeff.eig_func_cos=eig_func_cos;
end

function [ roots ] = roots_1D( n,lambda)
%1D_ROOTS Summary of this function goes here
%   Detailed explanation goes here
f=@(x)(lambda^2*x.^2-1).*sin(x)-2*lambda*x.*cos(x);
roots=zeros(n,1);
for i=1:n
    roots(i)=bisection(f,pi*(i-1),pi*i);
end
end

function p = bisection(f,a,b)

% provide the equation you want to solve with R.H.S = 0 form.
% Write the L.H.S by using inline function
% Give initial guesses.
% Solves it by method of bisection.
% A very simple code. But may come handy
if a==0
    a=a+eps;
end
if f(a)*f(b)>0
    disp('Wrong choice bro')
else
    p = (a + b)/2;
    err = abs(f(p));
    while err > 1e-7
        if f(a)*f(p)<0
            b = p;
        else
            a = p;
        end
        p = (a + b)/2;
        err = abs(f(p));
    end
end
end