function [eig_func,eig_func_grad] = Analytic2DExpSum( n,sigma,lambda)
%% cov(x,y)=sigma^2*exp((abs(x1-y1)+abs(x2-y2))/lambda)
%
%%
m=min(ceil(n^(1/1.25)),300);
[ ~,~,coeff] = SGMM.KL.Analytic1DExp( m,sigma,lambda);

all_eig=coeff.eig_value*coeff.eig_value';
tmp=sort(all_eig(:),'descend');
trashold=tmp(n);

[idxi,idxj]=find(all_eig>=trashold);
vals=all_eig((idxj-1)*m+idxi);
[vals,idx]=sort(sqrt(vals),'descend');
idxi=idxi(idx);
idxj=idxj(idx);

idxi=idxi(1:n);
idxj=idxj(1:n);
vals=vals(1:n);

eig_func=cell(n,1);
eig_func_grad=cell(n,1);
for k=1:n
    i=idxi(k);
    j=idxj(k);
    eig_func{k}=@(x,y)vals(k)*(coeff.eig_func_sin(i)*sin(x*coeff.eig_func_x(i))+...
        coeff.eig_func_cos(i)*cos(x*coeff.eig_func_x(i))).*(coeff.eig_func_sin(j)*sin(y*coeff.eig_func_x(j))+...
        coeff.eig_func_cos(j)*cos(y*coeff.eig_func_x(j)));
    tmp=cell(2,1);
    tmp{1}=@(x,y)vals(k)*(coeff.eig_func_sin(i)*coeff.eig_func_x(i)*cos(x*coeff.eig_func_x(i))-...
        coeff.eig_func_cos(i)*coeff.eig_func_x(i)*sin(x*coeff.eig_func_x(i))).*(coeff.eig_func_sin(j)*sin(y*coeff.eig_func_x(j))+...
        coeff.eig_func_cos(j)*cos(y*coeff.eig_func_x(j)));
    tmp{2}=@(x,y)vals(k)*(coeff.eig_func_sin(i)*sin(x*coeff.eig_func_x(i))+...
        coeff.eig_func_cos(i)*cos(x*coeff.eig_func_x(i))).*(coeff.eig_func_sin(j)*coeff.eig_func_x(j)*cos(y*coeff.eig_func_x(j))-...
        coeff.eig_func_cos(j)*coeff.eig_func_x(j)*sin(y*coeff.eig_func_x(j)));
    eig_func_grad{k}=tmp;
end

end