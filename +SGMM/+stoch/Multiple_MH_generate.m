function [points_new,diameters_new] = Multiple_MH_generate(f,points,diameter,new_diam,N,M,start)
%MULTIPLE_MH_GENERATE Summary of this function goes here
%   Detailed explanation goes here
k=size(start,2);
proposal=@(x)randn(x,k);

points_new=zeros(M,k);
diameters_new=zeros(M,1);
for i=1:M   
   [positions,posteriors] = MH_avoid(f,[points;points_new],[diameter;diameters_new],proposal,N,start);
    [~,idx]=max(posteriors);
    new_pos=positions(idx,:);
    points_new(i,:)=new_pos;
    diameters_new(i)=new_diam;
    
end
end

