function [x] = normrnd(n,mu,sigma)
%% MY_NORMRND
% n
% mu
% sigma

%%
x=randn(n,length(mu));
x=x.*sigma;
x=x+mu;
end

