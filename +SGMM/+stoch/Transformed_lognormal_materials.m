function [G, g,matrices,constants] = Transformed_lognormal_materials(poly_setting,means,stds,basis_means,basis_stds)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if nargin<4
    basis_means=means*0;
    basis_stds=stds*0+1;
end

n=length(means);
constants=zeros(n,1);
matrices=cell(n,2);
poly_orders=max(poly_setting);

for i=1:n
    constants(i)=exp(means(i)+stds(i)^2/2);
    poly_mean=basis_means(i);
    poly_std=basis_stds(i);
    dist_mean=basis_means(i)+basis_stds(i)*stds(i);
    dist_std=basis_stds(i);
    [x, w] = SGMM.quad.GaussHermite(poly_orders(i)*2,dist_mean,dist_std);
    [He] = SGMM.poly.HermitePoly(poly_orders(i),x,poly_mean,poly_std);
    matrices{i,1}=He*diag(w)*He';
    matrices{i,1}=constants(i)*(matrices{i,1}+matrices{i,1}')/2;
end


for i=1:n
    constants(i)=exp(-means(i)+stds(i)^2/2);
    poly_mean=basis_means(i);
    poly_std=basis_stds(i);
    dist_mean=basis_means(i)-basis_stds(i)*stds(i);
    dist_std=basis_stds(i);
    [x, w] = SGMM.quad.GaussHermite(poly_orders(i)*2,dist_mean,dist_std);
    [He] = SGMM.poly.HermitePoly(poly_orders(i),x,poly_mean,poly_std);
    matrices{i,2}=He*diag(w)*He';
    matrices{i,2}=constants(i)*(matrices{i,2}+matrices{i,2}')/2;
end

G=cell(n,1);
for i=1:n
    tmp_mask = -eye(n);
    tmp_mask(:,i)=tmp_mask(:,i)+1;
    tmp_mask(tmp_mask==-1)=2;
    [G_local] = SGMM.stoch.general_compose_stoch_matrices(poly_setting,tmp_mask,matrices);
    G{i}=G_local{1};
    for j=2:n
        G{i}=G{i}+G_local{j};
    end
   % constants(i) = max(eigs(G{i}));
   % G{i} = G{i}/constants(i);
end
constants=ones(n,1);
g=cell(n,1);
for i=1:n
    g{i}= full(G{i}(:,1));
end
end

