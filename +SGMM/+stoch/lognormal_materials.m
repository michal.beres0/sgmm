function [G, g,matrices,constants] = lognormal_materials(poly_setting,means,stds,basis_means,basis_stds)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if nargin<4
    basis_means=means*0;
    basis_stds=stds*0+1;
end

n=length(means);
constants=zeros(n,1);
matrices=cell(n,1);
poly_orders=max(poly_setting);

for i=1:n
    constants(i)=exp(means(i)+stds(i)^2/2);
    poly_mean=basis_means(i);
    poly_std=basis_stds(i);
    dist_mean=basis_means(i)+basis_stds(i)*stds(i);
    dist_std=basis_stds(i);
    [x, w] = SGMM.quad.GaussHermite(poly_orders(i)*2,dist_mean,dist_std);
    [He] = SGMM.poly.HermitePoly(poly_orders(i),x,poly_mean,poly_std);
    matrices{i}=He*diag(w)*He';
    matrices{i}=(matrices{i}+matrices{i}')/2;
end

[G] = SGMM.stoch.compose_stoch_matrices(poly_setting,eye(n),matrices);
g=cell(n,1);
for i=1:n
    g{i}= full(G{i}(:,1));
end
end

