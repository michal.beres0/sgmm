function [points_new,PS] = sample_MCMC_norm(PS)
%MULTIPLE_MH_GENERATE Summary of this function goes here
%   Detailed explanation goes here


f=PS.f;
points=PS.points;
diameter=PS.diameter;
new_diam=PS.new_diam;
N=1000;
M=PS.proposal_samples;
if isempty(points)
    start=PS.mean_sub;
else
    start=points(end,:);
end

k=size(start,2);
proposal=@(x)PS.std_sub.*randn(x,k);

points_new=zeros(M,k);
diameters_new=zeros(M,1);

for i=1:M
    
     [positions,posteriors] = SGMM.stoch.MH_avoid(f,[points;points_new(1:(i-1),:)],...
         [diameter;diameters_new(1:(i-1),:)],proposal(N),N,start,PS.MCMC_avoid_alpha);
    %[positions,posteriors] = SGMM.stoch.MH_avoid_fast(PS.mean_sub,PS.std_sub,[points;points_new(1:(i-1),:)],[diameter;diameters_new(1:(i-1),:)],proposal(N),N,start);
    %[~,idx]=max(posteriors);
    idx=ceil(rand()*N*0.1+N*0.9);
    new_pos=positions(idx,:);
    points_new(i,:)=new_pos;
    %points_new(i,:)=SGMM.stoch.MH_avoid_direct(PS.f,[PS.points;points_new(1:(i-1),:)],[PS.diameter;diameters_new(1:(i-1),:)],N,PS.mean_sub,PS.std_sub);
    diameters_new(i)=new_diam;
    start=new_pos;
    
end
end