function [G] = general_compose_stoch_matrices(poly,mask,matrices)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
n=size(mask,1);
m=size(mask,2);
N=size(poly,1);
mdeg = max(poly(:))+1;
tmp_mat = eye(mdeg);
G=cell(n,1);
for i=1:n
    tmp=ones(N);
    for j=1:m
        if mask(i,j)>0
            tmp=tmp.*matrices{j,mask(i,j)}(poly(:,j)+1,poly(:,j)+1);
        else
            tmp=tmp.*tmp_mat(poly(:,j)+1,poly(:,j)+1);
        end
    end

    G{i}=sparse(tmp);
end
end