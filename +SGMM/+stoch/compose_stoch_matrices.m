function [G] = compose_stoch_matrices(poly,mask,matrices)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
n=size(mask,1);
G=cell(n,1);
max_in_dim=max(poly)+1;
for i=1:n
   loc_mask= mask(i,:)~=0;
   [~,ia,ic]=unique(poly(:,~loc_mask),'rows');
   mm=length(ic);
   nn=length(ia);
   A=sparse(ic,1:mm,ones(mm,1),nn,mm);
   C=A'*A;
   [ii,jj,~]=find(C);
   ii_locidx=poly(ii,loc_mask);
   jj_locidx=poly(jj,loc_mask);
   offsets=[1 cumprod(max_in_dim(loc_mask))];
   
   ii_fin=sum(ii_locidx.*offsets(1:end-1),2);
   jj_fin=sum(jj_locidx.*offsets(1:end-1),2);
   
   fin_idx=ii_fin+jj_fin*offsets(end)+1;
   vals=matrices{i}(fin_idx);
   G{i}=sparse(ii,jj,vals,size(poly,1),size(poly,1));
end
end