function [points_new,PS] = sample_MCMC_dir_norm(PS)
%MULTIPLE_MH_GENERATE Summary of this function goes here
%   Detailed explanation goes here


f=PS.f;
points=PS.points;
diameter=PS.diameter;
new_diam=PS.new_diam;
N=3000;
M=PS.proposal_samples;
if isempty(points)
    start=PS.mean_sub;
else
    start=points(end,:);
end

k=size(start,2);


points_new=zeros(M,k);
diameters_new=zeros(M,1);
for i=1:M
    
    proposals=PS.mean_sub+randn(N,k).*PS.std_sub*1.5;
    
    [val]=my_posterior(f,[points;points_new(1:(i-1),:)],[diameter;diameters_new(1:(i-1))],proposals);
    
    [~,idx]=max(val);
    new_pos=proposals(idx,:);
    points_new(i,:)=new_pos;
    diameters_new(i)=new_diam;  
end
end


function [val]=my_posterior(f,points,diameter,x)
val=f(x);
if ~isempty(points)
    tmp=zeros(size(points,1),size(x,1));
    for i=1:size(points,1)
        tmp(i,:)=1-exp(-sum((points(i,:)-x).^2,2)./(2*diameter(i).^2));
    end
    val=val.*min(tmp)';
end
end