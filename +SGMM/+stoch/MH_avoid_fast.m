function [positions,posteriors] = MH_avoid_fast(meanv,stdv,points,diameter,N,start)
%MH_AVOID Summary of this function goes here
%   Detailed explanation goes here
decisions=rand(N,1);
proposals=0.5*PS.std_sub.*randn(N,length(meanv));
positions=zeros(N,size(start,2));
posteriors=zeros(N,1);

positions(1,:)=start;
posteriors(1)=posterior(meanv,stdv,points,diameter,positions(1,:));

for i=2:N
    x=proposals(i,:)+positions(i-1,:);
    val=exp(-sum(((x-meanv).^2)./(stdv.^2*2),2));
    if ~isempty(points)
        val=val*min(1-exp(-sum((points-x).^2,2)./(2*diameter.^2)));
    end
   posteriors(i)=val;
    alpha=posteriors(i)/posteriors(i-1);
    if alpha > decisions(i)
        positions(i,:)=proposals(i,:)+positions(i-1,:);
    else
        positions(i,:)=positions(i-1,:);
        posteriors(i)=posteriors(i-1);
    end
end
end