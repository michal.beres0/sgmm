function [positions,posteriors] = MH_avoid(f,points,diameter,proposals,N,start,alpha_val)
%MH_AVOID Summary of this function goes here
%   Detailed explanation goes here
decisions=rand(N,1);

positions=zeros(N,size(start,2));
posteriors=zeros(N,1);

positions(1,:)=start;
posteriors(1)=posterior(f,points,diameter,positions(1,:),alpha_val);

for i=2:N
    posteriors(i)=posterior(f,points,diameter,proposals(i,:)+positions(i-1,:),alpha_val);
    alpha=posteriors(i)/posteriors(i-1);
    if alpha > decisions(i)
        positions(i,:)=proposals(i,:)+positions(i-1,:);
    else
        positions(i,:)=positions(i-1,:);
        posteriors(i)=posteriors(i-1);
    end
end

end

function [val]=posterior(f,points,diameter,x,alpha_val)
val=f(x);
if ~isempty(points)
    val=val*min(1-exp(-sum((points-x).^2,2)./(2*diameter.^2))).^(alpha_val);
end
end

function [val]=posterior2(f,points,diameter,x,alpha_val)
val=f(x);
if ~isempty(points)
    val=val*min(max(abs(exp(points)-exp(x)),[],2)./min(min(exp(points),[],2),min(exp(x)))).^(alpha_val);
end
end