function [outcome] = MH_avoid_direct(f,points,diameter,N,mean_sub,std_sub)
%MH_AVOID Summary of this function goes here
%   Detailed explanation goes here

dim_n = length(mean_sub);
points_new = randn(N,dim_n).*std_sub+mean_sub;


density=f(points_new);
%%
%points=exp(points);
%points_new=exp(points_new);
%%
points_n = size(points,1);

tmp = zeros(N,points_n);
for i=1:points_n
    
    
    %tmp(:,i)=max(abs(points_new-points(i,:)),[],2)./min(min(points_new,[],2),min(points(i,:)));
    
    tmp(:,i)=1-exp(-(sum((points_new-points(i,:)),2)/(sqrt(2)*diameter(i))).^2);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
posteriors = density.*min(tmp,[],2);
[posteriors_sorted,idx]=sort(posteriors);

posteriors_sorted=cumsum(posteriors_sorted);
posteriors_sorted=posteriors_sorted/posteriors_sorted(end);
idx_target=find(posteriors_sorted>rand(),1);

outcome=points_new(idx(idx_target),:);
end
