function [figure1] = plot_thesis(X_data, Y_data, Legend, scaleX, scaleY,type_style,labelx,labely)
mark_size=10;
N=length(Y_data);
lines_width=ones(N,1)*3;
alpha_val=1;
marker_face='auto';
if strcmp(type_style,"markers")
    Markers = {'o','square','^','v','d','p','>','<'};
    Lines = {':',':',':',':',':',':',':',':',':',':',':'};
    colors=linspace(0,0.7^2,N);
end

if strcmp(type_style,"markers3")
    Markers = {'o','square','^','v','d','p','>','<'};
    Lines = {':',':',':',':',':',':',':',':',':',':',':'};
    mark_size=6;
    lines_width(:)=1;
    colors=linspace(0,0.7^2,N);
    alpha_val=0.5;
    marker_face='none';
end

if strcmp(type_style,"mixed")
    Markers = {'n','n','o','v','d','p','>','<'};
    Lines = {'-','--','n',':',':',':',':',':',':',':',':'};
    colors=[0 0.7 0];
end
if strcmp(type_style,"markers2")
    Markers = {'o','square','v','o','square','v'};
    Lines = {':',':',':','--','--','--'};
    colors=linspace(0,0.7^2,ceil(N/3));
    colors(1)=0;
    colors=repmat(colors,3,1);
    colors=colors(:);
    mark_size=9;
end
if strcmp(type_style,"lines")
    Markers = {'n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n'};
    Lines = {'-','--',':','-','--',':','-','--',':','-','--',':'};
    colors=linspace(0,0.7^2,ceil(N/3));
    colors(1)=0;
    colors=repmat(colors,3,1);
    colors=colors(:);
end
if strcmp(type_style,"lines5")
    N=N-1;
    Markers = {'n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n'};
    Lines = {'-','-','--',':','-','--',':','-','--',':','-','--',':'};
    colors=linspace(0,0.7^2,ceil(N/3));
    colors(1)=0;
    colors=repmat(colors,3,1);
    colors=colors(:);
    colors=[0.7; colors];
    N=N+1;
    lines_width(1)=6;
end
if strcmp(type_style,"lines6")
    Markers = {'n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n'};
    Lines = {'-',':','--','--',':',':','-','--',':','-','--',':'};
    colors = [0 0 0.8 0.8 0.5].^2;
end
if strcmp(type_style,"lines2")
    Markers = {'n','n','n','.','n','n','n','.','n','n','n','.','n','n','n','.','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n'};
    Lines = {'-','--','-.',':','-','--','-.',':','-','--','-.',':','-','--','-.',':','-','--','-.',':',};
    colors=linspace(0,0.7^2,ceil(N/4));
    colors=repmat(colors,4,1);
    colors=colors(:);
    mark_size=15;
end

if strcmp(type_style,"lines3")
    Markers = {'n','n','n','n','n','n','n','n','n','n','n','.','n','n','n','.','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n'};
    Lines = {'-',':','-',':','-',':','-',':','-',':','-',':','-','--','-.',':','-','--','-.',':','-','--','-.',':','-','--','-.',':',};
    colors=linspace(0,0.7^2,ceil(N/2));
    colors=repmat(colors,2,1);
    colors=colors(:);
    mark_size=15;
end
if strcmp(type_style,"lines4")
    Markers = {'n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n'};
    Lines = {'-','-','-',':',':',':','-','--',':','-','--',':'};
    colors=[0 0.35 0.5 0 0.35 0.5];
    colors=colors(:);
end

% Create figure
figure1 = figure('Position',[1 39 881 609]);

% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');
xmin=inf;
xmax=-inf;
ymin=inf;
ymax=-inf;

for i=1:N
    if isempty(X_data)
        xtmp=1:(length(Y_data{i}));
    else
        xtmp=X_data{i};
    end
    plot(xtmp,Y_data{i},'DisplayName',Legend{i},'MarkerFaceColor',marker_face,...
        'MarkerSize',mark_size,...
        'Marker',Markers{i},...
        'LineWidth',lines_width(i),...
        'LineStyle',Lines{i},...
        'Color',[ones(1,3)*sqrt(colors(i))]);
    xmin=min(xmin,min(xtmp));
    ymin=min(ymin,min(Y_data{i}));
    xmax=max(xmax,max(xtmp));
    ymax=max(ymax,max(Y_data{i}));
end

% Create ylabel
ylabel({labely},'Interpreter','latex');

% Create xlabel
xlabel({labelx},'Interpreter','latex');


if strcmp(scaleX,"log")
    xmin=log(xmin);
    xmax=log(xmax);
    dist = xmax-xmin;
    xmin=exp(xmin-dist*0.02);
    xmax=exp(xmax+dist*0.02);
    a=ceil(log10(xmin));
    b=floor(log10(xmax));
    set(axes1,"XTick",10.^(a:b));
else
    dist = xmax-xmin;
    xmin=(xmin-dist*0.02);
    xmax=(xmax+dist*0.02);
    
    a=ceil(xmin);
    b=floor(xmax);
    tick_len=ceil((b-a)/11);
    e2=2.^(0:10);
    e5=5.^(0:10);
    mattick=e2'*e5;
    mattick(mattick<tick_len)=inf;
    [i,j]=find(mattick==min(mattick(:)));
    tick=e2(i)*e5(j);
    
    if rem(a,tick)~=0
        aa=a+(tick-rem(a,tick));
    else
        aa=a;
    end
    if rem(b,tick)~=0
        bb=b-rem(b,tick);
    else
        bb=b;
    end
    tmp = aa:tick:bb;
    %     if aa~=a
    %         tmp=[a tmp];
    %     end
    %
    %     if bb~=b
    %         tmp=[tmp b];
    %     end
    set(axes1,"XTick",tmp);
end

if strcmp(scaleY,"log")
    ymin=log(ymin);
    ymax=log(ymax);
    dist = ymax-ymin;
    ymin=exp(ymin-dist*0.02);
    ymax=exp(ymax+dist*0.02);
    
    a=ceil(log10(ymin));
    b=floor(log10(ymax));
    set(axes1,"YTick",10.^(a:b));
else
    dist = ymax-ymin;
    ymin=(ymin-dist*0.02);
    ymax=(ymax+dist*0.02);
end

% Uncomment the following line to preserve the X-limits of the axes
xlim(axes1,[xmin xmax]);
% Uncomment the following line to preserve the Y-limits of the axes
ylim(axes1,[ymin ymax]);


%set(axes1,"XTick",[1e-6,1e-5,1e-4])

box(axes1,'on');
% Set the remaining axes properties
set(axes1,'FontSize',28,'XGrid','on','XMinorTick','on','XScale',scaleX,...
    'YGrid','on','YMinorTick','on','YScale',scaleY);
% Create legend
legend(axes1,'show');
legend(Legend,'Interpreter','latex')
set(figure1,'PaperType','<custom>');

set(figure1,'PaperUnits','points');
set(figure1,'PaperSize',[880*0.85 570*0.85]);
figure1.Renderer='Painters';

if strcmp(scaleY,"log")
    figure1.Children(2).YAxis.MinorTickValues=10.^(-16:16);
end

if strcmp(scaleX,"log")
    figure1.Children(2).XAxis.MinorTickValues=10.^(-16:16);
end
