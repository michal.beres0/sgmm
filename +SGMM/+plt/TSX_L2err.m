function [L2err] = TSX_L2err(u1,u2)
%TSX_L2ERR Summary of this function goes here
%   Detailed explanation goes here

data=load('+SGMM/+mat/TSX.mat');
TSX=data.TSX;
node = TSX.nodes;
elem = TSX.elems;

u0 = TSX.u0;
freeNode=u0*0+1;
freeNode(TSX.freeNode)=0;
freeNode=freeNode==1;

u1_all=u0;
u2_all=u0;
u1_all(freeNode)=u1;
u2_all(freeNode)=u2;
diff = u1_all-u2_all;


NT=size(elem,1);    % number of elements

%% VECTORIZATION (EDGES + AREA)
ve=zeros(NT,2,3);
ve(:,:,1)=node(elem(:,3),:)-node(elem(:,2),:);
ve(:,:,2)=node(elem(:,1),:)-node(elem(:,3),:);
ve(:,:,3)=node(elem(:,2),:)-node(elem(:,1),:);

area=0.5*abs(-ve(:,1,3).*ve(:,2,2)+ve(:,2,3).*ve(:,1,2));

a=diff(elem(:,3));
b=diff(elem(:,2));
c=diff(elem(:,1));
mean = (a+b+c)/3;

L2err=sqrt(sum((mean.^2).*area));

end

