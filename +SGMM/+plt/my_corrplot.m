function [] = my_corrplot(data,n1,n2,labels)
figure
a=quantile(data(:),0.9999);
b=quantile(data(:),0.0001);

d1line=linspace(b,a,n1);
d2line=linspace(b,a,n2);

nn=size(data,2);

dat = cell(nn);

for i=1:nn
    for j=1:nn
        if i==j
            [aa,bb]=hist(data(:,i),d1line);
            tmp.aa=aa/sum(aa)/(a-b)*n1;
            tmp.bb=bb;
            dat{i,j}=tmp;
        else            
            h=histogram2(data(:,j),data(:,i),d2line,d2line,'DisplayStyle','tile','ShowEmptyBins','on');
            tmp.val = h.Values;
            tmp.xbin=(h.XBinEdges(2:end)+h.XBinEdges(1:(end-1)))/2;
            tmp.ybin=(h.YBinEdges(2:end)+h.YBinEdges(1:(end-1)))/2;  
            dat{j,i}=tmp;
        end       
    end
end

figure
%tt=colormap('gray');
%colormap(flipud(tt))

for i=1:nn
    for j=1:nn
        subplot(nn,nn,(i-1)*nn+j)
        tmp=dat{i,j};
        if i==j
            bar(tmp.bb,tmp.aa)
            set(gca,'FontSize',15)
            box on
            grid on
            xlim([b a])
        else
            
            %histogram2(data(:,j),data(:,i),d2line,d2line,'DisplayStyle','tile','ShowEmptyBins','on');
            imagesc(tmp.xbin,tmp.ybin,tmp.val)
            alpha 0.99
            set(gca,'Ydir','normal')
            set(gca,'FontSize',15)
            box on
            grid on
                
        end
        if i==nn
           xlabel(labels{j}); 
        end
        if j==1
           ylabel(labels{j}); 
        end
        
    end
end

end

