function TSX_plot(u_sol)
%TSX_PLOT Summary of this function goes here
%   Detailed explanation goes here
data=load('+SGMM/+mat/TSX.mat');
TSX=data.TSX;
node = TSX.nodes;
elem = TSX.elems;

u0 = TSX.u0;
freeNode=u0*0+1;
freeNode(TSX.freeNode)=0;
freeNode=freeNode==1;

u=u0;

u(freeNode)=u_sol;

figure

trisurf(elem,node(:,1),node(:,2),u,'LineStyle','none','FaceColor','interp')
view(2)

end

