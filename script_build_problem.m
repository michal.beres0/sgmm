%% ------------------------------------------------------------------------
prior_pdf=@(x)exp(-sum(((x-mean_sub).^2)./((std_sub).^2*2),2));
if problem_type=='g'
    [materials] = SGMM.mat.PartitionGRF(multiply, n_vars, sample);
end
if problem_type=='m'
    [materials] = SGMM.mat.PartitionMetis(multiply, n_vars, sample);
end
if problem_type=='w'
    [materials] = SGMM.mat.PartitionWhiteNoise(multiply, n_vars, sample);
end
[ poly_set ] = SGMM.poly.CompletePolyDegrees( n_vars,max_poly_degree);
[G,g,~,scale] = SGMM.stoch.lognormal_materials(poly_set,mean_sub,std_sub);
[K,k,u,freeNode,K0] = SGMM.FEM.assemble_FEM_Lab_Example(materials,multiply,scale);
%% ------------------------------------------------------------------------