function [] = plot_grid(tri_grid)
%PLOT_GRID Summary of this function goes here
%   Detailed explanation goes here
node=tri_grid.node;
elem=tri_grid.elem;
edge=tri_grid.edge;
txt_size=10;
figure
hold on
plot(node(:,1),node(:,2),'g.','MarkerSize',20);
for i=1:size(node,1)
    text(node(i,1),node(i,2),num2str(i),'Color','black','FontSize',txt_size);
end
for i=1:size(elem,1)
    tmp_p=node(elem(i,:),:);
    x=sum(tmp_p(:,1))/3;
    y=sum(tmp_p(:,2))/3;
    text(x,y,num2str(i),'Color','red','FontSize',txt_size);
end
for i=1:size(edge,1)
    tmp_p=node(edge(i,:),:);
    plot(tmp_p(:,1),tmp_p(:,2),'k-');
    x=sum(tmp_p(:,1))/2;
    y=sum(tmp_p(:,2))/2;
    text(x,y,num2str(i),'Color','blue','FontSize',txt_size);
end
for i=1:size(elem,1)
    for j=1:3
        or=tri_grid.elem_outer_normal_orientation(i,j);
        normal=tri_grid.edge_global_normal(tri_grid.elem2edge(i,j),:);
        size_e=tri_grid.edge_lengths(tri_grid.elem2edge(i,j));
        center=tri_grid.elem_centres(i,:);
        size_t=tri_grid.elem_sizes(i);
        dp=normal*or*(size_t/size_e*2)/3;
        if or==1
            quiver(center(1),center(2),dp(1),dp(2),0, 'MaxHeadSize',0.5,'color','black')
        else
            quiver(center(1),center(2),dp(1),dp(2),0, 'MaxHeadSize',0.5,'color','red')
        end
    end
end
end

