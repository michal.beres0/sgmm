function [res_M] = bilinear(weak_form,basis1,basis2,matdata,elem_data)
%BILINEAR Summary of this function goes here
%   Detailed explanation goes here
m=length(basis1);
n=length(basis2);
n_m=basis1{1}.size;
n_n=basis2{1}.size;

local_matrices_val=cell(m,n);
local_matrices_idxi=cell(m,n);
local_matrices_idxj=cell(m,n);
for i=1:m
    for j=1:n
        tmp_val=weak_form(basis1{i},basis2{j},matdata);
        local_matrices_idxi{i,j}=basis1{i}.loc;
        local_matrices_idxj{i,j}=basis2{j}.loc;
        local_matrices_val{i,j}=(tmp_val*elem_data.weights).*elem_data.sizes;
    end
end
idxi=cell2mat(reshape(local_matrices_idxi,m*n,1));
idxj=cell2mat(reshape(local_matrices_idxj,m*n,1));
val=cell2mat(reshape(local_matrices_val,m*n,1));

res_M=sparse(double(idxi),double(idxj),val,n_m,n_n);
end