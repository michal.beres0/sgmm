function [res_f] = linear(weak_form,basis1,matdata,elem_data)
%BILINEAR Summary of this function goes here
%   Detailed explanation goes here
m=length(basis1);
n_m=basis1{1}.size;
local_matrices_val=cell(m,1);
local_matrices_idxi=cell(m,1);
for i=1:m
    tmp_val=weak_form(basis1{i},matdata);
    local_matrices_idxi{i}=basis1{i}.loc;
    local_matrices_val{i}=(tmp_val*elem_data.weights).*elem_data.sizes;
end
idxi=cell2mat(local_matrices_idxi);
val=cell2mat(local_matrices_val);
res_f=full(sparse(idxi,idxi*0+1,val,n_m,1));
end