function [basis] = P1()
%P1 Summary of this function goes here
%   Detailed explanation goes here
basis.val=@(tg,qd)P1_val(tg,qd);
basis.dir=@(tg,dir_idx,dir_fun)P1_dir(tg,dir_idx,dir_fun);
basis.plot=@(u,tri_grid)P1_plot(u,tri_grid);
end

function [bf] = P1_val(tg,qd)
bf=cell(3,1);
for i=1:3
    idx_B=mod(i,3)+1;
    idx_C=mod(i-2,3)+1;
    a=(qd.P{idx_B,2}-qd.P{idx_C,2})./qd.elem_sizes/2;
    b=(qd.P{idx_C,1}-qd.P{idx_B,1})./qd.elem_sizes/2;
    c=(qd.P{idx_B,1}.*qd.P{idx_C,2}-qd.P{idx_B,2}.*qd.P{idx_C,1})./qd.elem_sizes/2;
    v.val=a.*qd.X{1}+b.*qd.X{2}+c;
    v.grad{1}=a;
    v.grad{2}=b;
    v.loc=tg.elem(qd.elem_idx,i);
    v.size=size(tg.node,1);
    bf{i}=v;
end

end

function [x,freeDOF] = P1_dir(tg,dir_idx,dir_fun)
%P1_DIR Summary of this function goes here
%   Detailed explanation goes here
idx_n=dir_idx.idx_n;
x=tg.node(idx_n,1);
y=tg.node(idx_n,2);
val=dir_fun(x,y);
freeDOF=true(size(tg.node,1),1);
freeDOF(idx_n)=0;
x=zeros(size(tg.node,1),1);
x(idx_n)=val;
end

function [f] = P1_plot(u,tri_grid)
%PLOT_RES Summary of this function goes here
%   Detailed explanation goes here
f=figure();
h=trisurf(tri_grid.elem,tri_grid.node(:,1),tri_grid.node(:,2),full(u));

h.EdgeColor = 'none';
colormap jet(1000)
axis equal
view(0,90)
colorbar
title('$P1$ elem.','Interpreter','latex')
end
