function [ tri_grid, bound_idx ] = rect_mesh(height, width, h_elem, w_elem, smoothing_boundary)
%% Creation mesh (nodes and elements) from rectangle positioned in (0,0)
% Input height and width of rectangle, number of elements in row - w_elem,
% number elements in column - h_elem.
% Output node matrix = position of verticles of mash
%        elem matrix = indexes of nodes creating elements
%        bdFlag matrix = for each element contain border-flag for each edge

%% nodes matrix creation
x1 = linspace(0,width,w_elem+1);
n2 = h_elem+1;
x2 = linspace(0,height,h_elem+1);
[X1,X2] = meshgrid(x1,x2);
node = [X1(:) X2(:)];
%% elem matrix creation
m = 2*w_elem*h_elem;
elem = zeros(m,3);
idx = 1;
temp=reshape( repmat( 1:h_elem, 2,1 ), 1, [] );
for i=1:w_elem
    first=temp+(i-1)*n2;    % vector of first verticles of elements
    second=[temp(2:end) (h_elem+1)]+i*n2; % second verticles
    third=temp+1+(i-1)*n2;  % third verticles
    third(1:2:end)=third(1:2:end)+n2;
    % together make elements of column in mesh
    elem(idx:(idx+h_elem*2-1),:)=[first'  second'  third'];
    idx=idx+h_elem*2;
end

%% edge - link between edges and nodes
me_hor=w_elem*(h_elem+1);
me_ver=(w_elem+1)*h_elem;
me_diag=w_elem*h_elem;
me=me_hor+me_ver+me_diag;

tmp=repmat((1:(h_elem+1):(h_elem+1)*(w_elem))',1,h_elem+1)+repmat(0:h_elem,w_elem,1);
edge_hor=[tmp(:) tmp(:)+h_elem+1];
tmp=repmat((1:h_elem)',1,w_elem+1)+repmat((0:(h_elem+1):(h_elem+1)*(w_elem)),h_elem,1);
edge_ver=[tmp(:) tmp(:)+1];
tmp=repmat((1:h_elem)',1,w_elem)+repmat((0:(h_elem+1):(h_elem+1)*(w_elem-1)),h_elem,1);
edge_diag=[tmp(:) tmp(:)+h_elem+2];
edge=[edge_hor;edge_ver;edge_diag];

tri_grid.node=node;
tri_grid.elem=elem;
tri_grid.edge=edge;
tri_grid.Lx=width;
tri_grid.Ly=height;


if smoothing_boundary==true
    tri_grid.node(:,1)=betacdf(tri_grid.node(:,1)/width,4,4)*width;
    tri_grid.node(:,2)=betacdf(tri_grid.node(:,2)/height,4,4)*height;
end
node=tri_grid.node;

%% bdFlag matrix creation
% bdFlag - in each cell (1-bottom,2-right,3-top,4-left)
bdD=cell(4,1);
bdD{1}=1:(h_elem+1):(h_elem+1)*(w_elem+1);
bdD{2}=((h_elem+1)*w_elem+1):(h_elem+1)*(w_elem+1);
bdD{3}=(h_elem+1):(h_elem+1):(h_elem+1)*(w_elem+1);
bdD{4}=1:(h_elem+1);
bdN=cell(4,1);
bdN{1}=1:w_elem;
bdN{2}=me_hor+h_elem*w_elem+(1:h_elem);
bdN{3}=h_elem*w_elem+(1:w_elem);
bdN{4}=me_hor+(1:h_elem);

bound_idx.bdD=bdD;
bound_idx.bdN=bdN;
%% additional parameters
ve=zeros(m,2,3);
ve(:,:,1)=node(elem(:,3),:)-node(elem(:,2),:);
ve(:,:,2)=node(elem(:,1),:)-node(elem(:,3),:);
ve(:,:,3)=node(elem(:,2),:)-node(elem(:,1),:);

idx_e2p=repmat((1:size(edge,1))',2,1);
idx_p2e=edge(:);
e2p_i=sparse(idx_e2p,idx_p2e,idx_p2e*0+1,size(edge,1),size(node,1));

idx_t2p=repmat((1:m)',2,1);
edges_simple=[2 3;3 1;1 2];
tri_grid.elem2edge=zeros(m,3);
for i=1:3
    idx_p2t=elem(:,edges_simple(i,:));
    t2p_i=sparse(idx_t2p,idx_p2t,idx_p2t*0+1,m,size(node,1));
    tmp=t2p_i*e2p_i';
    [x,y,~]=find(tmp==2);
    [~,idx]=sort(x);
    tri_grid.elem2edge(:,i)=y(idx);
end

B=sparse(repmat((1:m)',3,1),tri_grid.elem2edge(:),tri_grid.elem2edge(:)*0+1,m,size(edge,1));
idx_border=sum(B)'==1;
[idxi,idxj,~]=find(B(:,~idx_border));
[~,idx]=sort(idxj);
edge2elem=zeros(size(edge,1),2);
edge2elem(~idx_border,:)=reshape(idxi(idx),2,sum(~idx_border))';

[idxi,idxj,~]=find(B(:,idx_border));
[~,idx]=sort(idxj);
edge2elem(idx_border,1)=idxi(idx);
tri_grid.edge2elem=edge2elem;
tri_grid.elem_centres = (node(elem(:,1),:)+node(elem(:,2),:)+node(elem(:,3),:))/3;
tri_grid.elem_sizes = 0.5*abs(-ve(:,1,3).*ve(:,2,2)+ve(:,2,3).*ve(:,1,2));
tri_grid.edge_lengths = sqrt((node(edge(:,1),1)-node(edge(:,2),1)).^2+(node(edge(:,1),2)-node(edge(:,2),2)).^2);
tri_grid.edge_global_normal = [(node(edge(:,1),2)-node(edge(:,2),2))./tri_grid.edge_lengths -(node(edge(:,1),1)-node(edge(:,2),1))./tri_grid.edge_lengths];
tri_grid.edge_center =[(node(edge(:,1),1)+node(edge(:,2),1)) (node(edge(:,1),2)+node(edge(:,2),2))]/2;

tri_grid.elem_outer_normal_orientation = zeros(m,3);

for i=1:3
    glo_norm=tri_grid.edge_global_normal(tri_grid.elem2edge(:,i),:);
    loc_out_norm=[-(node(elem(:,edges_simple(i,1)),2)-node(elem(:,edges_simple(i,2)),2)) (node(elem(:,edges_simple(i,1)),1)-node(elem(:,edges_simple(i,2)),1))];
    tri_grid.elem_outer_normal_orientation(:,i)=sign(glo_norm(:,1).*loc_out_norm(:,1)+glo_norm(:,2).*loc_out_norm(:,2));
end
edge2elem_orientation=zeros(size(edge,1),2);
for i=1:2
    idx_edge=edge2elem(:,i)>0;
    idx_elem=edge2elem(idx_edge,i);
    glo_norm=tri_grid.edge_global_normal(idx_edge,:);
    edge_mid=tri_grid.edge_center(idx_edge,:);
    triange_mid=tri_grid.elem_centres(idx_elem,:);
    loc_out_norm=edge_mid-triange_mid;
    edge2elem_orientation(idx_edge,i)=sign(glo_norm(:,1).*loc_out_norm(:,1)+glo_norm(:,2).*loc_out_norm(:,2));
end
tri_grid.edge2elem_orientation=edge2elem_orientation;
end