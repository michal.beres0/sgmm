function [qd] = quad_point_triangles(tg, poly_degree)
%QUAD_PREPARATION Summary of this function goes here
%   Detailed explanation goes here
Q = TEFEM.quad.quad_triangle(poly_degree,'Type','nonproduct','Domain',[0 0;1 0; 0 1]);
m=size(Q.Points,1);
n=size(tg.elem,1);
qd.elem_idx=(1:n)';
for i=1:3
    for j=1:2
        qd.P{i,j}=repmat(tg.node(tg.elem(qd.elem_idx,i),j),1,m);
        qd.n{i,j}=repmat(tg.edge_global_normal(tg.elem2edge(qd.elem_idx,i),j),1,m);
    end
    qd.sigma{i}=repmat(tg.elem_outer_normal_orientation(qd.elem_idx,i),1,m);
    qd.se{i}=repmat(tg.edge_lengths(tg.elem2edge(qd.elem_idx,i)),1,m);
end

xo{1}=repmat(Q.Points(:,1)',n,1);
xo{2}=repmat(Q.Points(:,2)',n,1);

qd.X{1}=(qd.P{2,1}-qd.P{1,1}).*xo{1}+(qd.P{3,1}-qd.P{1,1}).*xo{2}+qd.P{1,1};
qd.X{2}=(qd.P{2,2}-qd.P{1,2}).*xo{1}+(qd.P{3,2}-qd.P{1,2}).*xo{2}+qd.P{1,2};

qd.elem_sizes=repmat(tg.elem_sizes(qd.elem_idx),1,m);
qd.sizes=tg.elem_sizes(qd.elem_idx);
qd.weights=Q.Weights*2;
end