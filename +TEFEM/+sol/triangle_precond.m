function operator=triangle_precond(mat_orig,free_node,l,r,Wtype)
mat=cell(2);
if Wtype==1
    W=speye(size(mat_orig{1,2},2));
end
W_r=W/r;
A_r=mat_orig{1,1}+r*mat_orig{1,2}*inv(W)*mat_orig{2,1};
mat{1,1}=A_r;
mat{1,2}=l*mat_orig{1,2};
mat{2,2}=-W_r;
n=size(mat,1);
sizes=zeros(n);
for i=1:n
    for j=1:n
        sizes(i,j)=size(mat{i,j},1);
    end
end
sizes=max(sizes,[],2);
for i=1:n
    if size(free_node{i},1)==0
        free_node{i}=true(sizes(i),1);
    end
end
idx1=1:sum(free_node{1});
idx2=(1:sum(free_node{2}))+sum(free_node{1});
operator=@(xx)precond(mat{1,1}(free_node{1},free_node{1}),mat{1,2}(free_node{1},free_node{2}),mat{2,2}(free_node{2},free_node{2}),xx,idx1,idx2);
end

function y=precond(A,B,C,xx,idx1,idx2)
xx1=xx(idx1);
xx2=xx(idx2);
yy2=C\xx2;
yy1=A\(xx1-B*yy2);
y=[yy1;yy2];
end