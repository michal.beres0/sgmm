function operator=block_mult_op(mat,free_node)
n=size(mat,1);
sizes=zeros(n);
for i=1:n
    for j=1:n
        sizes(i,j)=size(mat{i,j},1);
    end
end
sizes=max(sizes,[],2);
for i=1:n
    if size(free_node{i},1)==0
        free_node{i}=true(sizes(i),1);
    end
    for j=1:n
        if size(mat{i,j},1)==0
            mat{i,j}=sparse(sizes(i),sizes(j));
        end
    end
end
fd=cell2mat(free_node);
A=cell2mat(mat);
A=A(fd,fd);
operator=@(x)A*x;
end