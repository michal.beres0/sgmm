function [b,u,fd,idx] = build_vectors_iter_sol(mat,rhs,x,free_node)
%DIRECT_BLOCK Summary of this function goes here
%   Detailed explanation goes here
n=size(mat,1);
sizes=zeros(n);
for i=1:n
    for j=1:n
        sizes(i,j)=size(mat{i,j},1);
    end
end
sizes=max(sizes,[],2);
for i=1:n
    if size(rhs{i},1)==0
        rhs{i}=zeros(sizes(i),1);
    end
    if size(x{i},1)==0
        x{i}=zeros(sizes(i),1);
    end
    if size(free_node{i},1)==0
        free_node{i}=true(sizes(i),1);
    end
end


b=cell2mat(rhs);
u=cell2mat(x);
fd=cell2mat(free_node);
idx=[0; cumsum(sizes)];

end