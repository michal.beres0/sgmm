function [res] = dot(A,B)
%DOT Summary of this function goes here
%   Detailed explanation goes here
n=length(A);
res=A{1}.*B{1};
for i=2:n
    res=res+A{i}.*B{i};
end
end