%% ------------------------------------------------------------------------
K_scales=ones(n_vars,1);
PS=SGMM.SGM.encapsulate_problem_setting(K,k,G,g,K0,poly_set,u,...
    freeNode,mean_sub,std_sub,n_vars,scale,prior_pdf,diameter,...
    proposal_samples,iparsols,K0_type,alphas,K_scales,weight_style);
if precise_sol_comp
    [PS] = SGMM.SGM.solve_SGM_system(PS);
end

if type_build=='m'
    if type_sampler=='m'
        proposal=@(x)SGMM.stoch.sample_MC_norm(x);
    end
    if type_sampler=='a'
        proposal=@(x)SGMM.stoch.sample_MCMC_norm(x);
    end
    if type_sampler=='d'
        proposal=@(x)SGMM.stoch.sample_MCMC_dir_norm(x);
    end
    solver = @(x,y)SGMM.SGM.sol_MC_direct(x,y);
    [PS] = SGMM.SGM.start_RB_MC(PS,solver);
end

if type_build=='s'
    solver = @(x,y)SGMM.SGM.sol_MC_direct(x,y);
    %[PS] = SGM.start_RB_MC(PS,solver);
end


if type_build=='r'
    if orthogonarizer_type=='S'
        orthogonalizer=@(V,idx_orig,W)SGMM.SGM.my_orth_SVD( V,idx_orig,W );
    end
    if orthogonarizer_type=='G'
        orthogonalizer=@(V,idx_orig,W)SGMM.SGM.my_orth_extended( V,idx_orig,W );
    end
    proposal=@(x)SGMM.SGM.RRKS_prop_standard(x);
    solver = @(x,y)SGMM.SGM.sol_RRKS_direct(x,y);
    
    [PS] = SGMM.SGM.set_RRKS_start_V(PS,type_start_RRKS,orthogonalizer);
    [PS] = SGMM.SGM.solve_RB_system(PS);
    [PS] = SGMM.SGM.weights_from_sol(PS);
end

if type_build=='b'
    if orthogonarizer_type=='S'
        orthogonalizer=@(V,idx_orig,W)SGMM.SGM.my_orth_SVD( V,idx_orig,W );
    end
    if orthogonarizer_type=='G'
        orthogonalizer=@(V,idx_orig,W)SGMM.SGM.my_orth_extended( V,idx_orig,W );
    end
    proposal=@(x)SGMM.SGM.RRKS_prop_standard(x);
    solver = @(x,y)SGMM.SGM.sol_RRKS_direct(x,y);
    if type_sampler=='m'
        proposal_MC=@(x)SGMM.stoch.sample_MC_norm(x);
    end
    if type_sampler=='a'
        proposal_MC=@(x)SGMM.stoch.sample_MCMC_norm(x);
    end
    if type_sampler=='d'
        proposal_MC=@(x)SGMM.stoch.sample_MCMC_dir_norm(x);
    end
    solver_MC = @(x,y)SGMM.SGM.sol_MC_direct(x,y);
    [PS] = SGMM.SGM.set_RRKS_start_V(PS,type_start_RRKS,orthogonalizer);
    [PS] = SGMM.SGM.solve_RB_system(PS);
    [PS] = SGMM.SGM.weights_from_sol(PS);
end

if type_build=='l'
    if orthogonarizer_type=='S'
        orthogonalizer=@(V,idx_orig,W)SGMM.SGM.my_orth_SVD( V,idx_orig,W );
    end
    if orthogonarizer_type=='G'
        orthogonalizer=@(V,idx_orig,W)SGMM.SGM.my_orth_extended( V,idx_orig,W );
    end
    proposal=@(x)SGMM.SGM.RRKS_prop_standard(x);
    solver = @(x,y)SGMM.SGM.sol_RRKSL_direct(x,y);
    
    [PS] = SGMM.SGM.set_RRKSL_start_V(PS,type_start_RRKS,alphas,K_scales,orthogonalizer);
    [PS] = SGMM.SGM.solve_RB_system_L(PS);
    [problem_setting] = SGMM.SGM.weights_from_sol(problem_setting);
end
%% ------------------------------------------------------------------------
