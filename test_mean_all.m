multiply=1;
for problem_type='g'
    for sample=1
        for n_vars=[5]
            max_poly_degree=4;
            rb_iter=100;
            proposal_samples=n_vars*10;
            iparsols=n_vars;
            
            rb_eps=1e-12;
            
            relres_all={};
            rel_x_all={};
            J_iter=1;
            for type_build='m'        % m - MC, r - RRKS, l - LRRKS, b - combined
                for rng_mat=1:30
                    rng(rng_mat)
                    mean_sub=randn(1,n_vars)*3;
                    std_sub=ones(1,n_vars)*0.2;
                    diameter=std_sub(1);
                    
                    
                    
                    % RRKS params
                    type_start_RRKS='a';    % a - all, s - sum, m - mean
                    K0_type='m';            % o - ones, m - mean
                    %type_build='r';         % m - MC, r - RRKS, l - LRRKS, b - combined
                    type_sampler='m';       % m - mc, a - avoid, d - direct avoid
                    orthogonarizer_type='S';% S - SVD, G - gramm-shmidt
                    weight_style='s';       % s - from sol, w - from orthogonaliser, n - none
                    precise_sol_comp=0;
                    
                    alpha_mult=2;
                    %% ------------------------------------------------------------------------
                    script_build_problem
                    %% ------------------------------------------------------------------------
                    alphas=ones(n_vars,1)*alpha_mult;
                    %% ------------------------------------------------------------------------
                    script_prepare_solvers
                    %% ------------------------------------------------------------------------
                    
                    for i=(1:rb_iter)
                        %PS.new_diam=PS.new_diam+i;
                        if type_build=='m'
                            [PS] = SGM.enrich_RB_MC(PS,proposal,solver);
                        end
                        if type_build=='r'
                            [PS] = SGM.enrich_RB_RRKS(PS,proposal,solver,orthogonalizer);
                        end
                        
                        if type_build=='b'
                            [PS] = SGM.enrich_RB_mixed(PS,proposal,solver,orthogonalizer,proposal_MC,solver_MC);
                        end
                        
                        if type_build=='l'
                            [PS] = SGM.enrich_RB_RRKSL(PS,proposal,solver,orthogonalizer);
                            [PS] = SGM.solve_RB_system_L(PS);
                            [PS] = SGM.compute_residual_L(PS);
                        end
                        
                        if type_build=='m' || type_build=='r' || type_build=='b'
                            
                            [PS] = SGM.solve_RB_system(PS);
                            [PS] = SGM.compute_residual(PS);
                        end
                        
                        [PS] = SGM.weights_from_sol(PS);
                        
                        fprintf('%d: %d\n',i,PS.last_tol)
                        if PS.last_tol<rb_eps
                            break;
                        end
                    end
                    PS.rel_res(end+1:rb_iter)=0;
                    PS.rel_x(end+1:rb_iter)=0;
                    relres_all{J_iter}=PS.rel_res;
                    rel_x_all{J_iter}=PS.rel_x;
                    J_iter=J_iter+1;
                end
                
                filename=['mean_all_' problem_type type_build '.mat'];
   %             save(filename,'relres_all','rel_x_all','-v7.3');
                
            end
        end
    end
end
