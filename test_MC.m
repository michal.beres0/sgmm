multiply=1;
for n_vars=[3]
    iparsols=1;
    for proposal_samples=[100]
        for problem_type='g'
            for sample=1
                
                max_poly_degree=4;
                rb_iter=50;
                for type_sampler='a'     % m - mc, a - avoid, d - direct avoid
                    
                    
                    relres_all={};
                    rel_x_all={};
                    J_iter=1;
                    for rng_seed=1:100
                        rb_eps=1e-12;
                        mean_sub=linspace(-15,-6,n_vars);
                        std_sub=ones(1,n_vars)*1;
                        diameter=std_sub(1);
                        
                        %% ------------------------------------------------------------------------
                        script_build_problem
                        %% ------------------------------------------------------------------------
                        
                        % RRKS params
                        alphas=ones(n_vars,1)*2;
                        type_start_RRKS='a';    % a - all, s - sum, m - mean
                        K0_type='m';            % o - ones, m - mean
                        type_build='m';         % m - MC, r - RRKS, l - LRRKS, b - combined
                        
                        orthogonarizer_type='S';% S - SVD, G - gramm-shmidt
                        weight_style='s';       % s - from sol, w - from orthogonaliser, n - none
                        precise_sol_comp=0;
                        
                        %% ------------------------------------------------------------------------
                        script_prepare_solvers
                        %% ------------------------------------------------------------------------
                        rng(rng_seed)
                        for i=(1:rb_iter)
                            %PS.new_diam=PS.new_diam+i;
                            if type_build=='m'
                                [PS] = SGM.enrich_RB_MC(PS,proposal,solver);
                            end
                            if type_build=='r'
                                [PS] = SGM.enrich_RB_RRKS(PS,proposal,solver,orthogonalizer);
                            end
                            
                            if type_build=='b'
                                [PS] = SGM.enrich_RB_mixed(PS,proposal,solver,orthogonalizer,proposal_MC,solver_MC);
                            end
                            
                            if type_build=='l'
                                [PS] = SGM.enrich_RB_RRKSL(PS,proposal,solver,orthogonalizer);
                                [PS] = SGM.solve_RB_system_L(PS);
                                [PS] = SGM.compute_residual_L(PS);
                            end
                            
                            if type_build=='m' || type_build=='r' || type_build=='b'
                                
                                [PS] = SGM.solve_RB_system(PS);
                                [PS] = SGM.compute_residual(PS);
                            end
                            
                            [PS] = SGM.weights_from_sol(PS);
                            
                            fprintf('%d: %d\n',i,PS.last_tol)
                            if PS.last_tol<rb_eps
                                break;
                            end
                        end
                        PS.rel_res(end+1:rb_iter)=0;
                        PS.rel_x(end+1:rb_iter)=0;
                        relres_all{J_iter}=PS.rel_res;
                        rel_x_all{J_iter}=PS.rel_x;
                        J_iter=J_iter+1;
                    end
                    
                    filename=['MCmvsaproposals31_' type_sampler num2str(proposal_samples) '.mat'];
                    save(filename,'relres_all','rel_x_all','-v7.3');
                end
            end
        end
    end
end

