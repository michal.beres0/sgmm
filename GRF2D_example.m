n=10;
nx=300;
sigma=1;
lambda=0.2;
[ eig_val,eig_grad ] = SGMM.KL.Analytic2DExpSum( n,sigma,lambda );

x = linspace(0, 1, nx);
y = linspace(0, 1, nx);
[xx, yy] = meshgrid(x, y);

base = zeros(nx*nx, n);
for i=1:n
    tmp = eig_val{i}(xx,yy);
    base(:,i) = tmp(:);
end

imagesc(reshape(base*randn(n,1),nx,nx))
colorbar
